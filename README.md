# tmss_gui_layouts_eval

This repository contains the POC applications developed for **TMSS GUI Layout** selection using open source **React** template and libraries.

There are two applications developed for comparison. One is developed using **airframe-react** an open source react template. The source code of this application is avaliable in the the folder **tmss-airframe-react**. The original demo of the template can be found in [**View Demo**](http://dashboards.webkom.co/react/airframe/). The source code of the template can be found [**here**](https://github.com/0wczar/airframe-react).

The second application is developed using [**PrimeReact**](https://primefaces.org/primereact/) a rich open source react component library developed by **Primefaces**. The library components and their documentation is available as [**showcase**](https://primefaces.org/primereact/showcase/#/). The source code of the template can be found [**here**](https://github.com/primefaces/primereact).

To run the applications, checkout the code, open the application folder and run the following commands.

1. npm install
2. npm start

The instrctions provided in the each application folder also can be followed to run the respective applications.