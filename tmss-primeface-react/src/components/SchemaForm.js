import React,{Component} from "react";
import ReactDOM from "react-dom";
import Form from "./formGenerationEngine";
import "./SchemaForm.css";


const schema = 
{
  type: "object",
  required: ["Stations", "Antenna", "Filter", "AnalogPointing","Beams","Duration","Correlator"],
  properties: {
    Stations: {
        type: "string",
        title: "Station List",
        oneOf: [
          {
            type: "string",
            title: "Fixed",
            enum: ["Fixed"]
          },
          {
            type: "string",
            title: "Dynamic",
            enum: ["Dynamic"]
          }
        ]
      },
        Antenna: {
          type: "string",
          title: "Antenna",
          
          enum: [
            "HBA_DUAL",
            "HBA_DUAL_INNER",
            "HBA_ONE",
            "HBA_ONE_INNER",
            "HBA_ZERO",
            "HBA_ZERO_INNER",
            "LBA_INNER",
            "LBA_OUTER",
            "LBA_SPARSE_EVEN",
            "LBA_SPARSE_ODD",
            "LBA_ALL"
          ]
        },
        Filter: {
          type: "string",
          title: "Band-pass filter",
          
          enum: [
            "LBA_10_70",
            "LBA_30_70",
            "LBA_10_90",
            "LBA_30_90",
            "HBA_110_190",
            "HBA_210_250"
          ]
        },
        AnalogPointing: {
          type: "string",
          title:"Analog Pointing"
        },
        
        Beams: {
              type: "string",
              title: "Beams",
        },
        Duration: {
          type: "number",
          title: "Duration",
          default: 300,
          minimum: 1
        },
     }
};

const uiSchema = {
 
 
};






const onSubmit = ({ formData }) => alert("Data submitted: ", formData);
 function SchemaForm()
 {return <Form schema={schema} onSubmit={onSubmit} uiSchema={uiSchema} />;}

 const rootElement = document.getElementById("root");
ReactDOM.render(<SchemaForm />, rootElement);
    
export default SchemaForm;



