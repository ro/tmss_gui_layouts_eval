import React, { Component } from 'react';
import { TreeTable } from 'primereact/treetable';
import { Column } from 'primereact/column';
import { TaskService } from '../service/TaskService';
import {Button} from 'primereact/button';
import {Toolbar} from 'primereact/toolbar';
import {TieredMenu} from 'primereact/tieredmenu';

export class Task extends Component {

    constructor(props) {
        super(props);
        this.state = {
            val1: null,
            nodes: [],
            selectedCustomers: null,
        };
       
        this.items = [
           
            {
                label:'Add Project',
                icon:'pi pi-fw pi-plus'
            },]
            
            
            
        this.taskservice = new TaskService();
        this.actionBodyTemplate = this.actionBodyTemplate.bind(this);
    }
    componentDidMount() {
        this.taskservice.gettasklist().then(data => this.setState({nodes: data}));
    }
    actionBodyTemplate() {
        return (
            
            <Button type="button" icon="pi pi-cog" className="p-button-secondary" onClick={(event) => this.menu.toggle(event)} aria-haspopup={true} aria-controls="overlay_tmenu"></Button>
        )
    }
    render() {
        return (
            
                <div>
                     
                <Toolbar>
                <div className="p-toolbar-group-left">
                <label><h5><b>Task List</b></h5></label>
                </div>
                <div className="p-toolbar-group-right">
                <Button  icon="pi pi-plus" style={{marginRight:'.25em'}} onClick={event =>{window.location = '/AddCycle#'}}/>
                </div>
                </Toolbar>
                <TreeTable  value={this.state.nodes} 
                paginator rows={10} emptyMessage="No cycles found" 
                paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown" rowsPerPageOptions={[10,25,50]}>
                <Column field="id" header="ID" headerStyle={{width: '5em', textAlign: 'center'}} />
                <Column field="priority" header="Priority" headerStyle={{width: '6em', textAlign: 'center'}} />
                <Column field="title" header="Title and Description"  />
                <Column field="update" header="Update"  headerStyle={{width: '8em', textAlign: 'center'}} />
                <Column header="Action" body={this.actionBodyTemplate} headerStyle={{width: '6em', textAlign: 'center'}} bodyStyle={{textAlign: 'center', overflow: 'visible'}} />
                </TreeTable>
                <TieredMenu model={this.items} popup={true} ref={el => this.menu = el} id="overlay_tmenu" />
                </div>
            
        );
    }
}
export default Task;