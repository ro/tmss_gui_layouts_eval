import React, { Component } from 'react';
import { TreeTable } from 'primereact/treetable';
import { Column } from 'primereact/column';
import { CustomerService } from '../service/CustomerService';
import {Button} from 'primereact/button';
import {Toolbar} from 'primereact/toolbar';
export class Cycle extends Component {

    constructor(props) {
        super(props);
        this.state = {
            val1: null,
            nodes: [],
            selectedCustomers: null,
        };
       
       
            
        this.customerservice = new CustomerService();
        this.actionBodyTemplate = this.actionBodyTemplate.bind(this);
    }
    componentDidMount() {
        this.customerservice.getcyclelist().then(data => this.setState({nodes: data}));
    }
    actionBodyTemplate() {
        return (
            <Button type="button" icon="pi pi-cog" className="p-button-secondary"></Button>
        );
    }
    render() {
        return (
            
                <div>
                     
                <Toolbar>
                <div className="p-toolbar-group-left">
                <label><h5>CycleList</h5></label>
                </div>
                <div className="p-toolbar-group-right">
                <Button icon="pi pi-search" style={{marginRight:'.25em'}} />
                <Button  icon="pi pi-plus" style={{marginRight:'.25em'}} onClick={event =>{window.location = '/AddCycle#'}}/>
                <Button icon="pi pi-trash" style={{marginRight:'.25em'}} />
                </div>
                </Toolbar>
                <TreeTable  value={this.state.nodes} selectionMode="checkbox" 
                paginator rows={10} emptyMessage="No cycles found" 
                paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown" rowsPerPageOptions={[10,25,50]}>
   
                <Column field="name" header="Name" sortable={true} filter filterPlaceholder="" />
                <Column field="start" header="Start" sortable={true} filter filterPlaceholder=""  />
                <Column field="stop" header="Stop"  sortable={true}filter filterPlaceholder="" />
                <Column field="stdhrs" header="Standard Hrs" sortable={true} filter filterPlaceholder="" headerStyle={{width: '10em', textAlign: 'center'}}/>
                <Column field="experthrs" header="Expert Hrs" sortable={true} filter filterPlaceholder="" />
                <Column field="filterhrs" header="Filter Hrs" sortable={true} filter filterPlaceholder="" />
                <Column field="projectid" header="Project Id" sortable={true} filter filterPlaceholder="" />
                <Column header="Action" body={this.actionBodyTemplate} headerStyle={{width: '8em', textAlign: 'center'}} bodyStyle={{textAlign: 'center', overflow: 'visible'}} />
                </TreeTable>
                
                </div>
            
        );
    }
}
