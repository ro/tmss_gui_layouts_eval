import React, { Component } from 'react';
import { TreeTable } from 'primereact/treetable';
import { Column } from 'primereact/column';
import { NodeService } from '../service/NodeService';
import {Button} from 'primereact/button';
import {Toolbar} from 'primereact/toolbar';
import { checkRolePermission } from './utilities';


export class Project extends Component {

    constructor(props) {
        super(props);
        this.state = {
            nodes: []
        };
        this.nodeservice = new NodeService();
         }

         privateBodyTemplate(rowData) {
            // return <span className={classNames('customer-badge', 'status-' + rowData.status)}>{rowData.status}</span>;
            console.log(rowData.private);
            return <i className={(rowData.private='pi pi-check')}></i>;
        }
       

    
    componentDidMount() {
        this.nodeservice.getTreeTableNodes().then(data => this.setState({nodes: data}));
    }

    render() {
        return (
                <div>
                <Toolbar>
                <div className="p-toolbar-group-left">
                <label><h5><b>Project</b></h5></label>
                </div>
                <div className="p-toolbar-group-right">
                <Button icon="pi pi-search" style={{marginRight:'.25em'}} />
                
                <Button  icon="pi pi-plus" style={{marginRight:'.25em'}} onClick={event =>{window.location = '/Task#'}}/>
                { checkRolePermission (' Project','view') && 
                <Button icon="pi pi-trash" style={{marginRight:'.25em'}} />
                }
                </div>
                </Toolbar>
                <TreeTable value={this.state.nodes}>
                    <Column field="name" header="Name" expander sortable={true}></Column>
                    <Column field="priority" header="Priority" sortable={true}></Column>
                    <Column field="tags" header="Tags" sortable={true}></Column>
                    <Column field="private" header="Private" body={this.privateBodyTemplate} sortable={true}> </Column>
                    <Column field="expert" header="Expert" sortable={true}></Column>
                    <Column field="filler" header="Filler" sortable={true}></Column>
                    <Column field="schedules" header="Schedules" sortable={true}></Column>
                </TreeTable>

           </div>     
        )
    }
}