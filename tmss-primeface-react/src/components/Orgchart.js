import 'primeicons/primeicons.css';
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.css';
import 'primeflex/primeflex.css';
import './Orgchart.scss';

import ReactDOM from 'react-dom';

import React, {Component} from 'react';
import {OrganizationChart} from 'primereact/organizationchart';

export class Orgchart extends Component {

    constructor() {
        super();
        this.state = {
            selection: []
        };

        this.data1 = [{
            label: 'Project',
            type: 'person',
            className: 'p-person',
            expanded: true,
            data: {name:'DUPLLO UC1'},
            children: [
                      {
                            label: 'Scheduling Sets',
                            type: 'person',
                            className: 'p-person',
                            expanded: true,
                            data: {name:'set of pointings'},
                        
                        },
                        {
                                
                            label: 'Scheduling Units',
                            type: 'person',
                            className: 'p-person',
                            expanded: true,
                            data: {name:'CTC+Pipelines'},

                    children:[{
                        label: 'Task',
                        className: 'department-cto',
                        expanded: true,
                        data:{name:'Observation + Plots'},
                        children:[
                            {
                                label: 'Sub Task',
                                className: 'department-cto',
                                expanded :true,
                                children:[
                                {
                                    label: 'Task Relation',
                                    className: 'department-cto',
                                    expanded :true,
                                    children:[
                                        {
                                            label: 'Data Products',
                                            className: 'department-cto',

                                        }
                                    ],

                                }]
                            }   
                            ],
                        
                    },
                   ]
                }
            ]
        }];

        

        this.nodeTemplate = this.nodeTemplate.bind(this);
    }

    nodeTemplate(node) {
        if (node.type === "person") {
            return (
                <div>
                    <div className="node-header">{node.label}</div>
                    <div className="node-content">
                        <img alt={node.data.avatar} src={`showcase/demo/images/organization/${node.data.avatar}`} srcSet="https://www.primefaces.org/wp-content/uploads/2020/05/placeholder.png" style={{ width: '32px' }}/>
                        <div>{node.data.name}</div>
                    </div>
                </div>
            );
        }

        if (node.type === "department") {
            return node.label;
        }
    }

    render() {
        return (
            <div className="organizationchart-demo">
                <h3>Advanced View</h3>
                <OrganizationChart value={this.data1} nodeTemplate={this.nodeTemplate} selection={this.state.selection} selectionMode="multiple"
                    onSelectionChange={event => this.setState({selection: event.data})} className="company"></OrganizationChart>

                
            </div>
        )
    }
}
                
const rootElement = document.getElementById("root");
ReactDOM.render(<Orgchart/>, rootElement);
export default Orgchart;