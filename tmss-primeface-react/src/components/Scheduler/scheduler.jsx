
import React, {Component} from 'react';
import moment from 'moment';
import _ from 'lodash';

import ScheduleData from './scheduleData';
import Timeline from './timeline';
import {customItemRenderer, customGroupRenderer} from './customRenderers';
import {Dropdown} from 'primereact/dropdown';

import 'primeflex/primeflex.css';
import {Button} from 'primereact/button';
import {InputText} from 'primereact/inputtext';
import {Calendar} from 'primereact/calendar';
import {Checkbox} from 'primereact/checkbox';
import {Dialog} from 'primereact/dialog';
import {Growl} from 'primereact/growl';

import './style.css';

const {TIMELINE_MODES} = Timeline;

const ITEM_DURATIONS = [moment.duration(6, 'hours'), moment.duration(12, 'hours'), moment.duration(18, 'hours')];

// Moment timezones can be enabled using the following
// import moment from 'moment-timezone';
// moment.locale('en-au');
// moment.tz.setDefault('Australia/Perth');

export class Scheduler extends Component {
  constructor(props) {
    super(props);
    
//    const startDate = moment('2018-08-31');
      const startDate = moment().startOf("day");
    //const endDate = startDate.clone().add(4, 'days');
    const endDate = moment().endOf("day");
    this.state = {
      selectedItems: [],
      rows: 7,
      items_per_row: 1,
      snap: 5,
      itemHeight: 125,
      startDate,
      endDate,
      message: '',
      timelineMode: TIMELINE_MODES.SELECT | TIMELINE_MODES.DRAG | TIMELINE_MODES.RESIZE,
      schedFormVisible: false,
      newSchedule: {},
      organizedSchedules: [],
      addedSchedules: [],
      showConfirmation: false,
      taskDetailsVisible: false,
      taskDetails: {}
    };
    this.reRender = this.reRender.bind(this);
    this.zoomIn = this.zoomIn.bind(this);
    this.zoomOut = this.zoomOut.bind(this);
    this.prevWeek = this.prevWeek.bind(this);
    this.nextWeek = this.nextWeek.bind(this);
    this.showNewScheduleForm = this.showNewScheduleForm.bind(this);
    this.setNewScheduleValues = this.setNewScheduleValues.bind(this);
    this.saveNewSchedule = this.saveNewSchedule.bind(this);
    this.validateNewSchedule = this.validateNewSchedule.bind(this);
    this.closeConfirmation = this.closeConfirmation.bind(this);
    this.cancelNewSchedule = this.cancelNewSchedule.bind(this);
    this.showTaskDetails = this.showTaskDetails.bind(this);
    this.hideTaskDetails = this.hideTaskDetails.bind(this);
    this.toggleCustomRenderers = this.toggleCustomRenderers.bind(this);
    this.toggleSelectable = this.toggleSelectable.bind(this);
    this.toggleDraggable = this.toggleDraggable.bind(this);
    this.toggleResizable = this.toggleResizable.bind(this);
  }

  componentWillMount() {
    this.reRender();
  }

  reRender() {
    const list = [];
    const groups = [];
    // const {snap} = this.state;

    this.key = 0;

    const viewDate = this.state.startDate;
    const weekStart = this.state.startDate.clone().startOf("week");
    const weekEnd = this.state.startDate.clone().endOf("week");
    const noOfDays = weekEnd.diff(weekStart, 'days');
    for (let i=0; i<=noOfDays; i++) {
        const groupDay = weekStart.clone().add(i,'days');
        groups.push({"id": i, title: groupDay.format("DD ddd"), day: groupDay});
    }
      
    
     const schedules = ScheduleData.schedules;
     console.log(schedules);
     let organizedSchedules = this.organizeSchedules(schedules);
     const ownerColors = ScheduleData.ownerColors;
     let keyID = this.key;
     this.setState({organizedSchedules: organizedSchedules});
      organizedSchedules.forEach(function(schedule) {
        const scheduleStartDay = schedule.ADST.format("DD ddd");
        const scheduleGroup = _.find(groups, {"title": scheduleStartDay});
        if (scheduleGroup) {
            let timelineItem = {};
            keyID += 1;
            timelineItem.key = keyID; 
            timelineItem.color = ownerColors[schedule.owner];
            timelineItem.row = scheduleGroup.id;
            timelineItem.start = viewDate.clone().hours(schedule.ADST.hours()).minutes(schedule.ADST.minutes());
            //timelineItem.end = moment(timelineItem.start).add(moment.duration(schedule.duration.value, schedule.duration.units));
            timelineItem.end = viewDate.clone().hours(schedule.ADSP.hours()).minutes(schedule.ADSP.minutes());
            timelineItem.ADST = schedule.ADST;
            timelineItem.ADSP = schedule.ADSP;
            timelineItem.title = schedule.name;
            timelineItem.owner = schedule.owner;
            timelineItem.international = schedule.international;
            timelineItem.duration = schedule.duration;
            timelineItem.status = schedule.status;
            list.push(timelineItem);
        }
     });
      this.state.addedSchedules.forEach(schedule => {
         list.push(schedule); 
      });
      this.key = list.length;
      
    // this.state = {selectedItems: [11, 12], groups, items: list};
    this.forceUpdate();
    this.setState({items: list, groups});
  }
    
  /**
   * Function to arrange the schedules in row and adjust the timings to avoid overlap
   * @param {*} schedules 
   */
  organizeSchedules(schedules) {
      const sortedSchedules = _.sortBy(schedules, function(schedule) { return moment(schedule.start);}) ;
      const organizedSchedules = [];
      let prevSchedule = null;
      sortedSchedules.forEach( sortSchedule => {
          let schedule = Object.assign({}, sortSchedule);
          //Set Expected Start and Stop without considering previous schedules
          schedule.EST = moment(schedule.start);
          schedule.ESP = schedule.EST.clone().add(schedule.duration.value, schedule.duration.units);
          
          //Set Adjusted Start and Stop based on previous schedule ESP/ADSP/ASP
          if (prevSchedule) {
              if (prevSchedule.ASP) {
                  schedule.ADST = schedule.EST<prevSchedule.ASP?prevSchedule.ASP.clone():schedule.EST.clone();
              } else {
                  schedule.ADST = schedule.EST<prevSchedule.ADSP?prevSchedule.ADSP.clone():schedule.EST.clone();
              }
              
          } else {
              schedule.ADST = moment(schedule.start);
          } 
          schedule.ADSP = schedule.ADST.clone().add(schedule.duration.value, schedule.duration.units);
          let childSchedule = null;
          if (schedule.ADST.clone().endOf("day") < schedule.ADSP) {
              childSchedule = Object.assign({}, schedule);
              schedule.ADSP = schedule.ADST.clone().endOf("day");
              childSchedule.ADST = childSchedule.ADSP.clone().startOf("day");
          }
          
          prevSchedule = schedule;
          organizedSchedules.push(schedule);
          if (childSchedule) {
              organizedSchedules.push(childSchedule);
              prevSchedule = childSchedule;
          }
      });
      return organizedSchedules;
  }

  handleRowClick = (e, rowNumber, clickedTime, snappedClickedTime) => {
    const message = `Row Click row=${rowNumber} @ time/snapped=${clickedTime.toString()}/${snappedClickedTime.toString()}`;
    this.setState({selectedItems: [], message});
  };

  zoomIn() {
    let currentMins = this.state.endDate.diff(this.state.startDate, 'minutes');
    let newMins = currentMins / 2;
    newMins = newMins<60? 60:newMins
    this.setState({endDate: this.state.startDate.clone().add(newMins, 'minutes')});
  }
  
  zoomOut() {
    let currentMins = this.state.endDate.diff(this.state.startDate, 'minutes');
    let newMins = currentMins * 2;
    let newEnd = this.state.startDate.clone().add(newMins, 'minutes');
    let stopLimit = this.state.startDate.clone().endOf("day");
    newEnd = newEnd>stopLimit?stopLimit:newEnd;
    this.setState({endDate: newEnd});
  }
  
  prevWeek() {
      this.state.startDate.add(-1, "weeks");
      this.state.endDate.add(-1, "weeks");
      this.reRender();
  }
  
  nextWeek() {
      this.state.startDate.add(1, "weeks");
      this.state.endDate.add(1, "weeks");
      this.reRender();
  }

  showNewScheduleForm(rowNumber, start) {
      const grpDay = _.find(this.state.groups, function(grp) { return grp.id == rowNumber;});
      const newStart = grpDay.day.clone().hours(start.hours()).minutes(start.minutes());
      this.setState({newSchedule: {row:rowNumber,  name: '', duration: 30, 
                                    viewDate: start, ADST: newStart.clone(), ADSP: newStart.clone().add(30, 'minutes'),
                                   start: newStart, end: newStart.clone().add(30, 'minutes'), international: false,
                                   overlap: false}});
      this.setState({schedFormVisible:true});
  }

  setNewScheduleValues(prop, value) {
      const newSchedule = this.state.newSchedule;
      newSchedule[prop] = value;
      if (prop === "start" || prop === "duration") {
          newSchedule.end = newSchedule.start.clone().add(newSchedule.duration, 'minutes');
          newSchedule.ADST.hours(newSchedule.start.hours);
          newSchedule.ADSP = newSchedule.ADST.clone().add(newSchedule.duration, 'minutes');
      }
      this.setState({newschedule: newSchedule});
  }

  saveNewSchedule() {
    this.key++;
    const ownerColor = ScheduleData.ownerColors[this.state.newSchedule.owner];
    const newStart = this.state.newSchedule.viewDate.clone().hours(this.state.newSchedule.start.hours()).minutes(this.state.newSchedule.start.minutes());
    const newEnd = newStart.clone().add(this.state.newSchedule.duration, 'minutes');
      const item = {
        key: this.key++,
        title: this.state.newSchedule.name,
        color: (ownerColor?ownerColor:'yellow'),
        row: this.state.newSchedule.row,
        start: newStart,
        end: newEnd,
        ADST: this.state.newSchedule.start,
        ADSP: this.state.newSchedule.end,
        owner: this.state.newSchedule.owner,
        duration: {value:this.state.newSchedule.duration, units: 'Minutes'},
        international: this.state.newSchedule.international,
        status: "DEFINED"
      };

      const newItems = _.clone(this.state.items);
      newItems.push(item);
      this.state.addedSchedules.push(item);
      this.setState({items: newItems});
      this.setState({schedFormVisible:false});
      this.setState({showConfirmation: false});
  }

  closeConfirmation() {
      this.setState({showConfirmation: false});
  }

  validateNewSchedule() {
      const newSchedule = this.state.newSchedule;
      const saveNewSchedule = this.saveNewSchedule;
      if (newSchedule.end > newSchedule.start.clone().endOf('day')) {
          this.growl.show({severity: 'error', summary: 'Unable to Save', detail: 'Schedule is continued next day also.'});
          return false;
      }
      const overlappingSchedules = _.filter(this.state.organizedSchedules, function(schedule) {
          if ( (newSchedule.ADST.isBetween(schedule.ADST, schedule.ADSP, 'minutes', '()')) ||
                (newSchedule.ADSP.isBetween(schedule.ADST, schedule.ADSP, 'minutes', '()')) ) {
              return true;
          }
          return false;
      });
      if (overlappingSchedules.length > 0) {
          let allowOverlap = false;
          this.setState({showConfirmation: true});
          return allowOverlap;
      } else {
          saveNewSchedule()
      }
      
  }

  cancelNewSchedule() {
      this.setState({schedFormVisible:false});
      this.setState({showConfirmation: false});
  }

  toggleCustomRenderers(checked) {
    this.setState({useCustomRenderers: checked});
  }

  toggleSelectable() {
    const {timelineMode} = this.state;
    let newMode = timelineMode ^ TIMELINE_MODES.SELECT;
    this.setState({timelineMode: newMode, message: 'Timeline mode change: ' + timelineMode + ' -> ' + newMode});
  }
  toggleDraggable() {
    const {timelineMode} = this.state;
    let newMode = timelineMode ^ TIMELINE_MODES.DRAG;
    this.setState({timelineMode: newMode, message: 'Timeline mode change: ' + timelineMode + ' -> ' + newMode});
  }
  toggleResizable() {
    const {timelineMode} = this.state;
    let newMode = timelineMode ^ TIMELINE_MODES.RESIZE;
    this.setState({timelineMode: newMode, message: 'Timeline mode change: ' + timelineMode + ' -> ' + newMode});
  }
  handleItemClick = (e, key) => {
    const message = `Item Click ${key}`;
    const {selectedItems} = this.state;

    let newSelection = selectedItems.slice();

    // If the item is already selected, then unselected
    const idx = selectedItems.indexOf(key);
    if (idx > -1) {
      // Splice modifies in place and returns removed elements
      newSelection.splice(idx, 1);
    } else {
      newSelection.push(Number(key));
    }

    this.setState({selectedItems: newSelection, message});
  };

  handleItemDoubleClick = (e, key) => {
    const message = `Item Double Click ${key}`;
    this.setState({message});
  };

  handleItemContextClick = (e, key) => {
    const message = `Item Context ${key}`;
    this.setState({message});
  };

  handleRowDoubleClick = (e, rowNumber, clickedTime, snappedClickedTime) => {
    const message = `Row Double Click row=${rowNumber} time/snapped=${clickedTime.toString()}/${snappedClickedTime.toString()}`;
    
    const randomIndex = Math.floor(Math.random() * Math.floor(ITEM_DURATIONS.length));

    let start = snappedClickedTime.clone();
    this.showNewScheduleForm(rowNumber, start);
  };

  handleRowContextClick = (e, rowNumber, clickedTime, snappedClickedTime) => {
    const message = `Row Click row=${rowNumber} @ time/snapped=${clickedTime.toString()}/${snappedClickedTime.toString()}`;
    this.setState({message});
  };

  handleInteraction = (type, changes, items) => {
    /**
     * this is to appease the codefactor gods,
     * whose wrath condemns those who dare
     * repeat code beyond the sacred 5 lines...
     */
    function absorbChange(itemList, selectedItems) {
      itemList.forEach(item => {
        let i = selectedItems.find(i => {
          return i.key == item.key;
        });
        if (i) {
          item = i;
          //item.title = moment.duration(item.end.diff(item.start)).humanize();
          item.duration = moment.duration(item.end.diff(item.start)).humanize();
        }
      });
    }

    switch (type) {
      case Timeline.changeTypes.dragStart: {
        return this.state.selectedItems;
      }
      case Timeline.changeTypes.dragEnd: {
        const newItems = _.clone(this.state.items);

        absorbChange(newItems, items);
        this.setState({items: newItems});
        break;
      }
      case Timeline.changeTypes.resizeStart: {
        return this.state.selectedItems;
      }
      case Timeline.changeTypes.resizeEnd: {
        const newItems = _.clone(this.state.items);

        // Fold the changes into the item list
        absorbChange(newItems, items);

        this.setState({items: newItems});
        break;
      }
      case Timeline.changeTypes.itemsSelected: {
        this.setState({selectedItems: _.map(changes, 'key')});
        break;
      }
      default:
        return changes;
    }
  };

  hideTaskDetails(){
    this.setState({taskDetails: {}, taskDetailsVisible: false});
  }

  showTaskDetails(taskDetails){
    this.setState({taskDetails: taskDetails, taskDetailsVisible: true});
  }

  ownerTemplate(option) {
    if(!option.value) {
      return option.label;
    }
    else {
        let ownerColor = ScheduleData.ownerColors[option.value];

        return (
            <div className="p-clearfix" style={{backgroundColor:ownerColor}}>
                <span style={{float:'right',margin:'.5em .25em 0 0'}}>{option.label}</span>
            </div>
        );
    }
  }

  render() {
    const {
      selectedItems,
      rows,
      items_per_row,
      snap,
      itemHeight,
      startDate,
      endDate,
      items,
      groups,
      message,
      useCustomRenderers,
      timelineMode,
      schedFormVisible,
      newSchedule,
    } = this.state;
    
    let newScheduleStartDate = newSchedule.start?newSchedule.start.toDate():null;
    let newScheduleEndDate = newSchedule.end?newSchedule.end.toDate():null;

    const rangeValue = [newSchedule.start, endDate];

    const selectable = (TIMELINE_MODES.SELECT & timelineMode) === TIMELINE_MODES.SELECT;
    const draggable = (TIMELINE_MODES.DRAG & timelineMode) === TIMELINE_MODES.DRAG;
    const resizeable = (TIMELINE_MODES.RESIZE & timelineMode) === TIMELINE_MODES.RESIZE;

    const rowLayers = [];
    for (let i = 0; i < rows; i += 1) {
      if (i % 5 === 0 && i !== 0) {
        continue;
      }
      let curDate = startDate.clone();
      while (curDate.isSameOrBefore(endDate)) {
        const dayOfWeek = Number(curDate.format('d')); // 0 -> 6: Sun -> Sat
        let bandDuration = 0; // days
        let color = '';
        if (dayOfWeek % 6 === 0) {
          color = 'blue';
          bandDuration = dayOfWeek === 6 ? 2 : 1; // 2 if sat, 1 if sun
        } else {
          color = 'green';
          bandDuration = 6 - dayOfWeek;
        }

        rowLayers.push({
          start: curDate.clone(),
          end: curDate.clone().add(bandDuration, 'days'),
          style: {backgroundColor: color, opacity: '0.3'},
          rowNumber: i
        });
        curDate.add(bandDuration, 'days');
      }
    }
    let owner=null, owners = [];

    for (owner in ScheduleData.ownerColors) {
      owners.push({label: owner, value: owner});
    }
    
    return (
        <div className="p-grid">
            
            <div className="p-col-12 p-md-12 p-lg-12">
                <Growl ref={(el) => this.growl = el} />
                <h2>Scheduler</h2>
                <div className="p-grid" style={{margin: 24}}>
                    <div className="p-col-12">
                        <Button onClick={this.zoomIn} label="Zoom in" style={{'marginRight': '5px'}}></Button>
                        <Button onClick={this.zoomOut} label="Zoom out"style={{'marginRight': '5px'}}></Button>
                        <Button onClick={this.prevWeek} label="Previous Week" style={{'marginRight': '5px'}} />
                        <Button onClick={this.nextWeek} label="Next Week" style={{'marginRight': '5px'}} />
                    </div>                    
                    <div className="p-col-12">
                        {/* <span>Debug: </span>
                        {message} */}
                    </div>
                </div>

                {/* Timeline component */}
                <Timeline
                    shallowUpdateCheck
                    items={items}
                    groups={groups}
                    startDate={startDate}
                    endDate={endDate}
                    rowLayers={rowLayers}
                    selectedItems={selectedItems}
                    timelineMode={timelineMode}
                    snapMinutes={snap}
                    itemHeight={itemHeight}
                    onItemClick={this.handleItemClick}
                    onItemDoubleClick={this.handleItemDoubleClick}
                    onItemContextClick={this.handleItemContextClick}
                    onInteraction={this.handleInteraction}
                    onRowClick={this.handleRowClick}
                    onRowContextClick={this.handleRowContextClick}
                    onRowDoubleClick={this.handleRowDoubleClick}
                    onTaskShow = {this.showTaskDetails}
                    onTaskHide = {this.hideTaskDetails}
                    itemRenderer={useCustomRenderers ? customItemRenderer : undefined}
                    groupRenderer={useCustomRenderers ? customGroupRenderer : undefined}
                    groupTitleRenderer={useCustomRenderers ? () => <div>Group title</div> : undefined}
                />
                {/* New Schedule form dialog */}
                <Dialog header="Add New Schedule" visible={schedFormVisible} style={{width: '50vw'}} 
                        modal={true} onHide={() => this.setState({schedFormVisible: false})}
                        footer={<div>
                            <Button key="back" onClick={this.cancelNewSchedule} label="Cancel" />
                            <Button key="submit" type="primary" onClick={this.validateNewSchedule} label="Save" />
                            </div>
                        }>
                
                    <div className="p-fluid">
                        <div className="p-field p-grid">
                            <label htmlFor="name" className="p-col-12 p-md-2">Name</label>
                            <div className="p-col-12 p-md-10">
                                <InputText id="name" type="text" value={newSchedule.name} onChange={e => this.setNewScheduleValues('name', e.target.value)}/>
                            </div>
                        </div>
                        <div className="p-field p-grid">
                            <label htmlFor="owner" className="p-col-12 p-md-2">Owner</label>
                            <div className="p-col-12 p-md-10">
                                {/* <InputText id="owner" type="text" value={newSchedule.owner} onChange={e => this.setNewScheduleValues('owner', e.target.value)} /> */}
                                <Dropdown value={newSchedule.owner} options={owners} onChange={e => this.setNewScheduleValues('owner', e.target.value)} itemTemplate={this.ownerTemplate}  style={{width: '12em'}}
                                          filter={true} filterPlaceholder="Select Owner" filterBy="label,value" showClear={true}/>
                            </div>
                        </div>
                        <div className="p-field p-grid">
                            <label htmlFor="duration" className="p-col-12 p-md-2">Duration (mins)</label>
                            <div className="p-col-12 p-md-10">
                                <InputText id="duration" value={newSchedule.duration} onChange={e => this.setNewScheduleValues('duration', e.target.value)} />
                            </div>
                        </div>
                        <div className="p-field p-grid">
                            <label htmlFor="start_date" className="p-col-12 p-md-2">Start Date</label>
                            <div className="p-col-12 p-md-10">
                                <Calendar showTime={true} hourFormat="24" value={newScheduleStartDate} dateFormat="dd-MM-yy" onChange={(e) => this.setNewScheduleValues('start', e.value)}></Calendar>
                            </div>
                        </div>
                        <div className="p-field p-grid">
                            <label htmlFor="end_date" className="p-col-12 p-md-2">End Date</label>
                            <div className="p-col-12 p-md-10">
                            <Calendar showTime={true} hourFormat="24" value={newScheduleEndDate} dateFormat="dd-MM-yy" onChange={(e) => this.setNewScheduleValues('end', e.value)}></Calendar>
                            </div>
                        </div>
                        <div className="p-field p-grid">
                            <label htmlFor="international" className="p-col-12 p-md-2">Is International?</label>
                            <div className="p-col-12 p-md-10">
                                <Checkbox checked={newSchedule.international} onChange={e => this.setNewScheduleValues('international', e.target.checked)}></Checkbox>
                            </div>
                        </div>
                    </div>
                </Dialog>

                {/* Confirmation Dialog if newschedule overlaps with existing schedule */}
                <Dialog header="Schedule Overlaps" visible={this.state.showConfirmation} style={{width: '50vw'}} 
                        modal={true} onHide={() => this.setState({showConfirmation: false})}
                        footer={<div>
                            <Button key="back" onClick={this.closeConfirmation} label="No"></Button>
                            <Button key="submit" type="primary" onClick={this.saveNewSchedule} label="Yes"></Button>
                            </div>
                        }>
                        <span>The current Scheduling time overlaps with an exisitng Schedule. Do you want to allow it?</span>
                </Dialog>
                
                {/* Task Details Dialog. Not used */}
                <Dialog header={this.state.taskDetails?this.state.taskDetails.owner:'Task Details'} 
                        visible={this.state.taskDetailsVisible} style={{width: '25vw'}} 
                        modal={false} onHide={() => this.hideTaskDetails()}>
                        <div onMouseOver={(e) => {this.setState({taskDetailsVisible: true})}} onMouseOut={(e) => {this.hideTaskDetails()}}>
                              <p><strong>Name: </strong>{this.state.taskDetails.title}</p>
                              <p><strong>Start: </strong>{this.state.taskDetails.ADST?this.state.taskDetails.ADST.format("DD-MMM-YYYY HH:mm"):''}</p>
                              <p><strong>End  : </strong>{this.state.taskDetails.ADSP?this.state.taskDetails.ADSP.format("DD-MMM-YYYY HH:mm"):''}</p>
                              <p><strong>Duration: </strong>{this.state.taskDetails.duration?this.state.taskDetails.duration.value:''} {this.state.taskDetails.duration?this.state.taskDetails.duration.units:''}</p>
                              <p><strong>International: </strong>{this.state.taskDetails.international && <i className="pi pi-check" />}{!this.state.taskDetails.international && <i className="pi pi-times" />}</p>
                        </div>
                </Dialog>
            </div>
        </div>
    );
    
  }
}
