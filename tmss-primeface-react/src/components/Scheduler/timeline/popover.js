import React, {Component} from 'react';
import PropTypes from 'prop-types';

import {OverlayPanel} from 'primereact/overlaypanel';
import {Card} from 'primereact/card';

export default class Popover extends Component {

    static propTypes = {
        type: PropTypes.string,
        iconClass: PropTypes.string,
        tooltip: PropTypes.string,
        title: PropTypes.string
    }

    constructor(props) {
        super(props);
        this.state = {
            popVisible: false
        };
        this.show = this.show.bind(this);
        this.hide = this.hide.bind(this);
    }

    show() {
        this.setState({popVisible: !this.state.popVisible});
    }

    hide() {
        this.setState({popVisible: false});
    }

    render() {
        return (
            <div style={{float:'right'}}>
                {/* <Button type="button" label="Toggle" onClick={(e) => this.op.toggle(e)}/> */}
                <span tooltip="View Details" onClick={(e) => {this.op.toggle(e)}}>
                    <i className="pi pi-question-circle" />
                </span>
                {/* <Button label="S" onClick={(e) => this.op.toggle(e)}/> */}
                <OverlayPanel ref={(el) => {this.op = el;}} id="overlay_panel" showCloseIcon={true}>
                    <Card title="Task Name" subTitle="Owner">
                        <div>
                            <p><strong>Name: </strong></p>
                            <p><strong>Start: </strong></p>
                            <p><strong>End  : </strong></p>
                            <p><strong>Duration: </strong></p>
                            <p><strong>International: </strong></p>
                        </div>
                    </Card>
                </OverlayPanel>
            </div>
        )
    }
}
