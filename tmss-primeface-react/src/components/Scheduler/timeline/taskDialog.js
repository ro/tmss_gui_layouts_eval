import React from 'react';
import {Table, Button, Modal} from 'antd';

const { Column, ColumnGroup } = Table;

const taskData = [
  {
    key: '1',
    name: 'Observation-1 Task-1',
    start: '25 May 2020',
    status: 'Finished',
    address: 'New York No. 1 Lake Park',
    tags: ['nice', 'developer'],
  },
  {
    key: '2',
   name: 'Observation-1 Task-2',
    start: '25 May 2020',
    status: 'ERROR',
    address: 'London No. 1 Lake Park',
    tags: ['loser'],
  },
  {
    key: '3',
    name: 'Observation-1 Task-3',
    start: '25 May 2020',
    status: 'Finished',
    address: 'Sidney No. 1 Lake Park',
    tags: ['cool', 'teacher'],
  },
];

export const DefaultItemRenderer = props => {
  const {taskListVisible, ...rest} = props;
  return (
    <Modal visible={taskListVisible} title="Task List" onCancel={hideTasks}
      footer={[
        <Button key="submit" type="primary" onClick={hideTasks}>
          Ok
        </Button>,
      ]}
    >
        <Table dataSource={taskData}>
            <Column title="Name" dataIndex="name" key="name" />
            <Column title="Started" dataIndex="start" key="start" />
            <Column title="Staus" dataIndex="status" key="status" />
        </Table>
    </Modal>    
  );
};
