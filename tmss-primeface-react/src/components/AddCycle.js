import React, { Component } from 'react';
import {InputText} from 'primereact/inputtext';
import {InputTextarea} from "primereact/inputtextarea";
import 'primeflex/primeflex.css';
import { Calendar } from 'primereact/calendar';
import {Dropdown} from 'primereact/dropdown';
import {Button} from 'primereact/button';

  


export class AddCycle extends Component{
    constructor() {
        super();
        let today = new Date();
        let month = today.getMonth();
        let year = today.getFullYear();
        let prevMonth = (month === 0) ? 11 : month - 1;
        let prevYear = (prevMonth === 11) ? year - 1 : year;
        let nextMonth = (month === 11) ? 0 : month + 1;
        let nextYear = (nextMonth === 0) ? year + 1 : year;
        this.state = {
            date: null
        };
        this.minDate = new Date();
        this.minDate.setMonth(prevMonth);
        this.minDate.setFullYear(prevYear);

        this.maxDate = new Date();
        this.maxDate.setMonth(nextMonth);
        this.maxDate.setFullYear(nextYear);

        this.invalidDates = [today];

        this.dateTemplate = this.dateTemplate.bind(this);
      }

      dateTemplate(date) {
        if (date.day > 10 && date.day < 15) {
            return (
                <div style={{ backgroundColor: '#1dcbb3', color: '#ffffff', fontWeight: 'bold', borderRadius: '50%', width: '2em', height: '2em', lineHeight: '2em', padding: 0 }}>{date.day}</div>
            );
        }
        else {
            return date.day;
        }
    }
     
     render(){
         return(
            
                    <div className="p-fluid p-formgrid p-grid">
                        <div className="p-field p-col-12 p-md-6">
                        <h2>AddCycle</h2>
                       </div>
                       <div className="p-field p-col-12 p-md-6">
                        
                       </div>
                        <div className="p-field p-col-12 p-md-6">
                        <label htmlFor="firstname6">Name</label>
                        <InputText id="firstname6" type="text" />
                    </div>
                    <div className="p-field p-col-12 p-md-6">
                        <label htmlFor="lastname6">Number</label>
                        <InputText id="lastname6" type="number" />
                    </div>
                    <div className="p-field p-col-12">
                        <label htmlFor="address">Description</label>
                        <InputTextarea id="address" type="text" rows="4" />
                    </div>
                    <div className="p-field p-col-12 p-md-6">
                        <label htmlFor="firstname6">Start</label>
                        <Calendar value={this.state.date} onChange={(e) => this.setState({ date: e.value })} showTime={true} showSeconds={true} />
                    </div>
                    <div className="p-field p-col-12 p-md-6">
                        <label htmlFor="lastname6">End</label>
                        <Calendar value={this.state.date} onChange={(e) => this.setState({ date: e.value })} showTime={true} showSeconds={true} />
                    </div>
                    <div className="p-field p-col-12 p-md-6">
                        <label htmlFor="lastname6">Standard Hrs</label>
                        <Dropdown  placeholder="Select Standard Hours"  style={{width: '12em'}}/>
                    </div>
                    <div className="p-field p-col-12 p-md-6">
                        <label htmlFor="lastname6">Expert Hrs</label>
                        <Dropdown  placeholder="Select Expert Hours"  style={{width: '12em'}}/>
                    </div>
                    <div className="p-field p-col-12 p-md-6">
                        <label htmlFor="lastname6">Filler Hrs</label>
                        <Dropdown  placeholder="Select Filler Hours"  style={{width: '12em'}}/>
                    </div>
                    <div className="p-field p-col-12 p-md-6">
                        <label htmlFor="firstname6">Tags</label>
                        <InputText id="firstname6"  type="text" />
                    </div>
                    
                    <div className ="p-field  p-md-6">
                    <Button type="button" label="Save" className="p-button-save"/>
                    </div>
                    <div className ="p-field ">
                    
                    </div>
                    <div className ="p-field p-md-6">
                    <Button type="button" label="Cancel" className="p-button-cancel"/>
                    </div>
                    </div>     
            
        );

    }
}