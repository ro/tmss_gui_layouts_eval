import 'primeicons/primeicons.css';
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.css';
import 'primeflex/primeflex.css';
import './FileUploadDemo.css';
import ReactDOM from 'react-dom';

import React, { Component } from 'react';
import {Growl} from 'primereact/growl';
import {FileUpload} from 'primereact/fileupload';

export class FileUploadDemo extends Component {

    constructor() {
        super();

        this.onUpload = this.onUpload.bind(this);
        this.onBasicUpload = this.onBasicUpload.bind(this);
        this.onBasicUploadAuto = this.onBasicUploadAuto.bind(this);
    }

    onUpload(event) {
        this.growl.show({severity: 'info', summary: 'Success', detail: 'File Uploaded'});
    }

    onBasicUpload(event) {
        this.growl.show({severity: 'info', summary: 'Success', detail: 'File Uploaded with Basic Mode'});
    }

    onBasicUploadAuto(event) {
        this.growl.show({severity: 'info', summary: 'Success', detail: 'File Uploaded with Auto Mode'});
    }

    render() {
        return (
            <div>
                <h3>File Upload</h3>
                <FileUpload name="demo[]" url="./upload.php" onUpload={this.onUpload}
                            multiple={true} accept="image/*" maxFileSize={1000000} />
                 <Growl ref={(el) => { this.growl = el; }}></Growl>
            </div>
        )
    }
}
                
const rootElement = document.getElementById("root");
ReactDOM.render(<FileUploadDemo />, rootElement);
export default FileUploadDemo;