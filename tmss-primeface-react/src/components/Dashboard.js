import React, { Component } from 'react';
import {Chart} from 'primereact/chart';
import {Column} from 'primereact/column';
import {CarService} from '../service/CarService';
import {DataTable} from 'primereact/datatable';
export class Dashboard extends Component {

    constructor(props) {
        super(props);
        this.state = {
          nodes: [],
            

             tasks: [],
            city: null,
            selectedCar: null,
            lineData: {
                labels: ['17-May-20', '18-May-20', '19-May-20', '20-May-20', '21-May-20', '22-May-20', '23-May-20'],
                datasets: [
                    {
                        label: 'Telescope Observation',
                        backgroundColor: [
                            '#EC407A',
                            '#AB47BC',
                            '#42A5F5',
                            '#7E57C2',
                            '#66BB6A',
                            '#FFCA28',
                            '#26A69A'
                        ],
                        data: [65, 59, 80, 81, 56, 55, 40],
                        fill:false,
                        borderColor: '#007be5'
                    },
                   
                ]
            },
                   
        };
        this.carservice = new CarService();
    }

    componentDidMount() {
      this.carservice.getCarsSmall().then(data => this.setState({cars: data}));
  }

  
   render() {        
        return (
            <div className="p-grid p-fluid dashboard">
                <div class="p-col-12 p-md-3 summary-box summary-box-projects">
                      <div class="card">
                          <center><div class="summary-box-title">Projects<div></div>
                          <i className="pi pi-folder" style={{'fontSize': '3em'}}></i><p><b>152</b></p>
                        </div></center>
                      </div>
                  </div>
                  <div class="p-col-12 p-md-3 summary-box summary-box-activeschedules">
                      <div class="card">
                          <center><div class="summary-box-title">Active Schedules<div></div>
                          <i className="pi pi-calendar" style={{'fontSize': '3em'}}></i><p><b>100</b></p>

                        </div></center>
                      </div>
                  </div>
                  <div class="p-col-12 p-md-3 summary-box summary-box-taskcompleted">
                      <div class="card">
                          <center><div class="summary-box-title">Task completed<div></div>
                          <i className="pi pi-pencil" style={{'fontSize': '3em'}}></i><p><b>300</b></p>
                        </div></center>
                      </div>
                  </div>
                  <div class="p-col-12 p-md-3 summary-box summary-box-activetask">
                      <div class="card">
                          <center><div class="summary-box-title">ActiveTask<div></div>
                          <i className="pi pi-spinner" style={{'fontSize': '3em'}}></i><p><b>80</b></p>
                        </div></center>
                      </div>
                  </div>
                  <div className="p-col-12 p-lg-6">
                    <div className="card">
                        <Chart type="bar" data={this.state.lineData}/>
                    </div>                    
               </div> 
               
               <div className="p-col-12 p-lg-6">
                    <div className="card">
                        <h1 style={{fontSize:'16px'}}>Scheduling Sets</h1>
                        <DataTable value={this.state.cars}  style={{marginBottom: '20px'}} responsive={true}
                          selectionMode="single" selection={this.state.selectedCar} onSelectionChange={(e) => this.setState({selectedCar: e.value})}>
                            <Column field="name" header="Name"  />
                            <Column field="cycle" header="Cycle"  />
                            <Column field="project" header="Project"  />
                            <Column field="units" header="Units" />
                        </DataTable>
               </div>     </div>

          
            </div>      
        );
    }
}
export default Dashboard;