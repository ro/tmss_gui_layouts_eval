import { authenticationService } from '../_services/authentication.service';



export function randomArray(arr) {
    const index = Math.round(Math.random() * (arr.length - 1));
    return arr[index];
}


export function checkRolePermission(modulename, attbname){
   
    if (authenticationService.currentUserValue) { 
        let rolearray = authenticationService.currentUserValue.results;
        if(rolearray && rolearray.length>0){
          const rolepermission = rolearray.find(x => x.module === modulename);
          if(rolepermission){
            if(attbname==='view')
                return rolepermission.view;
            else if(attbname === 'edit')
                return rolepermission.edit;
            else if(attbname === 'add')
                return rolepermission.add;
            else if(attbname === 'delete')
                return rolepermission.delete;
          }
        }
    }
    return false;
  }

  export function getCurrentUser(){
    if (authenticationService.currentUserValue) { 
        return authenticationService.currentUserValue;
    }
    return null;
  }

  export function getCurrentUserFirstName(){
    if (authenticationService.currentUserValue) { 
        return authenticationService.currentUserValue.firstName;
    }
    return "";
  }


  export function getCurrentUserLastName(){
    if (authenticationService.currentUserValue) { 
        return authenticationService.currentUserValue.lastName;
    }
    return "";
  }

  export function getCurrentUserRoleName(){
    if (authenticationService.currentUserValue) { 
        return authenticationService.currentUserValue.role;
    }
    return "";
  }
 