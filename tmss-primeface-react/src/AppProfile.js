
import React, { Component } from 'react';
import { authenticationService } from './_services';




export class AppProfile extends Component {

    constructor() {
        super();
        this.state = {
            expanded: false
        };
        this.onClick = this.onClick.bind(this);
    }

    onClick(event) {
        this.setState({expanded: !this.state.expanded});
        event.preventDefault();
    }
    render() {
        
            return  (
                 <div className="layout-profile">
                <div>
                    <img src="assets/layout/images/profile.png" alt="" />
                    <p>{authenticationService.currentUserValue.firstName}</p>
                </div>
               
            </div>
        );
    }
}