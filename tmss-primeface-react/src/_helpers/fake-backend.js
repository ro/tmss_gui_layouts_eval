import { Role } from './'


export function configureFakeBackend() {
    let users = [
        { id: 1, username: 'admin', password: 'admin', firstName: 'Admin', lastName: 'User', role: Role.Admin},
        { id: 2, username: 'user', password: 'user', firstName: 'Normal', lastName: 'User', role: Role.User },
        { id: 3, username: 'astronomer', password: 'astro', firstName: 'Astronomer', lastName: 'User', role: Role.Astronomer},
        { id: 4, username: 'operator', password: 'operator', firstName: 'Operator', lastName: 'User', role: Role.Operator},
        { id: 5, username: 'sos', password: 'sos', firstName: 'Sos', lastName: 'User', role: Role.Sos},
        
   ];
   

   const userRolePermissions = [ {"id": 1, "module": "Cycle", "role": "Astronomer", "add": true, "edit": true, "view": true, "delete": true},
                         {"id": 2, "module": "Cycle", "role": "Sos", "add": false, "edit": true, "view": true, "delete": true},
                         {"id": 3, "module": "Cycle", "role": "Operator", "add": true, "edit": false, "view": false, "delete": true},
                         {"id": 4, "module": "Cycle", "role": "Admin", "add": true, "edit": true, "view": true, "delete": true},
                         {"id": 5, "module": "Project", "role": "Astronomer", "add": true, "edit": true, "view": true, "delete": true},
                         {"id": 6, "module": "Project", "role": "Sos", "add": true, "edit": true, "view": false, "delete": true},
                         {"id": 7, "module": "Project", "role": "Operator", "add": true, "edit": true, "view": true, "delete": true},
                         {"id": 8, "module": "Project", "role": "Admin", "edit":true, "view": true, "delete": true},
                         {"id": 9, "module": "Scheduler", "role": "Astronomer", "add": true, "edit": true, "view": true, "delete": true},
                         {"id": 10, "module": "Scheduler", "role": "Sos", "add": true, "edit": true, "view": true, "delete": true},
                         {"id": 11, "module": "Scheduler", "role": "Operator", "add": true, "edit": true, "view": true, "delete": true},
                         {"id": 12, "module": "Scheduler", "role": "Admin", "add": true, "edit": true, "view": true, "delete": true},
                         {"id": 13, "module": "Task", "role": "Astronomer", "add": true, "edit": true, "view": false, "delete": true},
                         {"id": 14, "module": "Task", "role": "Sos", "add": true, "edit": true, "view": false, "delete": true},
                         {"id": 15, "module": "Task", "role": "Operator", "add": true, "edit": true, "view": true, "delete": true},
                         {"id": 16, "module": "Task", "role": "Admin", "add":true, "edit": true, "view": true, "delete": true},
                         {"id": 17, "module": "Reports", "role": "Astronomer", "add": true, "edit": true, "view": true, "delete": true},
                         {"id": 18, "module": "Reports", "role": "Sos", "add": true, "edit": true, "view": false, "delete": true},
                         {"id": 19, "module": "Reports", "role": "Operator", "add": true, "edit": true, "view": true, "delete": true},
                         {"id": 20, "module": "Reports", "role": "Admin", "add": true, "edit": true, "view": true, "delete": true},
                         {"id": 21, "module": "Dashboard", "role": "Astronomer", "add": true, "edit": true, "view": true, "delete": true},
                         {"id": 22, "module": "Dashboard", "role": "Sos", "add": true, "edit": true, "view": true, "delete": true},
                         {"id": 23, "module": "Dashboard", "role": "Operator", "add": true, "edit": true, "view": true, "delete": true},
                         {"id": 24, "module": "Dashboard", "role": "Admin", "add": true, "edit": true, "view": true, "delete": true},
                         {"id": 25, "module": "Calender", "role": "Astronomer", "add": true, "edit": true, "view": true, "delete": true},
                         {"id": 26, "module": "Calendar", "role": "Sos", "add": true, "edit": true, "view": true, "delete": true},
                         {"id": 27, "module": "Calendar", "role": "Operator", "add": true, "edit": true, "view": true, "delete": true},
                         {"id": 28, "module": "Calendar", "role": "Admin", "add": true, "edit": true, "view": true, "delete": true},
                         {"id": 29, "module": "Orgchart", "role": "Astronomer", "add": true, "edit": true, "view": true, "delete": true},
                         {"id": 30, "module": "Orgchart", "role": "Sos", "add": true, "edit": true, "view": true, "delete": true},
                         {"id": 31, "module": "Orgchart", "role": "Operator", "add": true, "edit": true, "view": true, "delete": true},
                         {"id": 32, "module": "Orgchart", "role": "Admin", "add": true, "edit": true, "view": true, "delete": true},
                         {"id": 23, "module": "FileUploadDemo", "role": "Astronomer", "add": true, "edit": true, "view": true, "delete": true},
                         {"id": 34, "module": "FileUploadDemo", "role": "Sos", "add": true, "edit": true, "view": true, "delete": true},
                         {"id": 35, "module": "FileUploadDemo", "role": "Operator", "add": true, "edit": true, "view": true, "delete": true},
                         {"id": 36, "module": "FileUploadDemo", "role": "Admin", "add": true, "edit": true, "view": true, "delete": true},
                         {"id": 37, "module": "JSONEditor", "role": "Astronomer", "add": true, "edit": true, "view": true, "delete": true},
                         {"id": 38, "module": "JSONEditor", "role": "Sos", "add": true, "edit": true, "view": true, "delete": true},
                         {"id": 39, "module": "JSONEditor", "role": "Operator", "add": true, "edit": true, "view": true, "delete": true},
                         {"id": 40, "module": "JSONEditor", "role": "Admin", "add": true, "edit": true, "view": true, "delete": true},]
                        
                         
                         
    
    
    let realFetch = window.fetch;
    window.fetch = function (url, opts) {
        const authHeader = opts.headers['Authorization'];
        const isLoggedIn = authHeader && authHeader.startsWith('Bearer fake-jwt-token');
        const roleString = isLoggedIn && authHeader.split('.')[1];
        const role = roleString ? Role[roleString] : null;

        return new Promise((resolve, reject) => {
            // wrap in timeout to simulate server api call
            setTimeout(() => {
                // authenticate - public
                if (url.endsWith('/users/authenticate') && opts.method === 'POST') {
                    const params = JSON.parse(opts.body);
                    const user= users.find(x => x.username === params.username && x.password === params.password);
            
                if (!user) return error('Username or password is incorrect'); 
            
                const userModulePermissions = userRolePermissions.filter(x => x.role === user.role)
                
                    return ok({
                        id: user.id,
                        username: user.username,    
                        firstName: user.firstName,
                        lastName: user.lastName,
                        role: user.role,
                        permissions: userModulePermissions,
                        token: `fake-jwt-token.${user.role}`
                    },
 );
                    

                
                }


                // get user by id - admin or user (user can only access their own record)
                if (url.match(/\/users\/\d+$/) && opts.method === 'GET') {
                    if (!isLoggedIn) return unauthorised();

                    // get id from request url
                    let urlParts = url.split('/');
                    let id = parseInt(urlParts[urlParts.length - 1]);

                    // only allow normal users access to their own record
                    const currentUser = users.find(x => x.role === role);
                    if (id !== currentUser.id && role !== Role.Admin) return unauthorised();
                    

                    
                    
                    const user = users.find(x => x.id === id);
                    return ok(user);

             }

                // get all users - admin only
                if (url.endsWith('/users') && opts.method === 'GET') {
                    if (role !== Role.Admin) return unauthorised();
                    return ok(users);
                }
                if (url.endsWith('/users') && opts.method === 'GET') {
                    if (role !== Role.Astronomer) return unauthorised();
                    return ok(users);
                }
                if (url.endsWith('/users') && opts.method === 'GET') {
                    if (role !== Role.Operator) return unauthorised();
                    return ok(users);
                }
                if (url.endsWith('/users') && opts.method === 'GET') {
                    if (role !== Role.Sos) return unauthorised();
                    return ok(users);
                }





                // pass through any requests not handled above
                realFetch(url, opts).then(response => resolve(response));

                // private helper functions

                function ok(body) {
                    resolve({ ok: true, text: () => Promise.resolve(JSON.stringify(body)) })
                }

                function unauthorised() {
                    resolve({ status: 401, text: () => Promise.resolve(JSON.stringify({ message: 'Unauthorised' })) })
                }

                function error(message) {
                    resolve({ status: 400, text: () => Promise.resolve(JSON.stringify({ message })) })
                }
            }, 500);
        });
    }
}