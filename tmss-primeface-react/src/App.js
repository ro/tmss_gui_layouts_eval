import React, {Component} from 'react';
import classNames from 'classnames';
import {AppTopbar} from './AppTopbar';
import {Router, Route} from 'react-router-dom';
import {Cycle} from './components/Cycle';
import {Project} from './components/Project';
import {Reports} from './components/Reports';
import {Calendar} from './components/Calendar';
import {Orgchart} from './components/Orgchart';
import {LoginPage} from './components/LoginPage';
import {AddCycle} from './components/AddCycle';
import {Dashboard} from './components/Dashboard';
import {Scheduler} from './components/Scheduler';
import FileUploadDemo from './components/FileUploadDemo';
import {AppMenu} from './AppMenu';
import {AppProfile} from './AppProfile';
import Task from './components/Task';
import {JSONEditor} from './components/JSONEditor/jsonEditor';
import 'primereact/datatable';
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';
import '@fullcalendar/core/main.css';
import '@fullcalendar/daygrid/main.css';
import '@fullcalendar/timegrid/main.css';
import './layout/layout.scss';
import './App.scss';
import { PrivateRoute } from './_components';
import { history, Role } from './_helpers';
import { authenticationService } from './_services';



class App extends Component {

    constructor() {
        super();
        this.state = {
            layoutMode: 'static',
            layoutColorMode: 'dark',
            staticMenuInactive: false,
            overlayMenuActive: false,
            mobileMenuActive: false,
            currentUser: null,
            isAdmin: false
        };

        this.onWrapperClick = this.onWrapperClick.bind(this);
        this.onToggleMenu = this.onToggleMenu.bind(this);
        this.onSidebarClick = this.onSidebarClick.bind(this);
        this.onMenuItemClick = this.onMenuItemClick.bind(this);
        this.logout = this.logout.bind(this);
        this.createMenu();
    }

    onWrapperClick(event) {
        if (!this.menuClick) {
            this.setState({
                overlayMenuActive: false,
                mobileMenuActive: false
            });
        }

        this.menuClick = false;
    }

    onToggleMenu(event) {
        this.menuClick = true;

        if (this.isDesktop()) {
            if (this.state.layoutMode === 'overlay') {
                this.setState({
                    overlayMenuActive: !this.state.overlayMenuActive
                });
            }
            else if (this.state.layoutMode === 'static') {
                this.setState({
                    staticMenuInactive: !this.state.staticMenuInactive
                });
            }
        }
        else {
            const mobileMenuActive = this.state.mobileMenuActive;
            this.setState({
                mobileMenuActive: !mobileMenuActive
            });
        }
       
        event.preventDefault();
    }

    onSidebarClick(event) {
        this.menuClick = true;
    }

    onMenuItemClick(event) {
        if(!event.item.items) {
            this.setState({
                overlayMenuActive: false,
                mobileMenuActive: false
            })
        }
    }

    createMenu() {
        
        this.menu = [
            
    
            {label: 'Dashboard', icon: 'pi pi-fw pi-home', command: () => {window.location = '/'}},
            {
                label: 'Cycle', icon: 'pi pi-fw pi-refresh', command: () => {window.location = '/Cycle'},
            },
            
            {
                label: 'Project', icon: 'pi pi-fw pi-folder',command: () => {window.location = '/Project'},
                
            },
            {
                label: 'Scheduler', icon: 'pi pi-fw pi-calendar',command: () => {window.location = '/Scheduler'},

            },
            {
                label: 'Task', icon: 'pi pi-fw pi-pencil',command: () => {window.location = '/Task'}
                
            },
            {
                label: 'Reports', icon: 'pi pi-fw pi-chart-bar',command: () => {window.location = '/Reports'}
                
            },
            {
                label: 'Calendar', icon: 'pi pi-fw pi-calendar',command: () => {window.location = '/Calendar'}

                
            },
            {
                label: 'Orgchart', icon: 'pi pi-fw pi-chart-bar',command: () => {window.location = '/Orgchart'}

                
            },
            {
                label: 'FileUploadDemo', icon: 'pi pi-fw pi-cloud-upload',command: () => {window.location = '/FileUploadDemo'}

                
            },
            {
                label: 'JSONEditor', icon: 'pi pi-fw pi-file',command: () => {window.location = '/JSONEditor'}

                
            },
           
            
            
            ];
    }

    addClass(element, className) {
        if (element.classList)
            element.classList.add(className);
        else
            element.className += ' ' + className;
    }

    removeClass(element, className) {
        if (element.classList)
            element.classList.remove(className);
        else
            element.className = element.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
    }

    isDesktop() {
        return window.innerWidth > 1024;
    }

    logout() {
        authenticationService.logout();
        history.push('/login');
    }

    componentDidMount() {
        authenticationService.currentUser.subscribe(x => this.setState({
            currentUser: x,
            isAdmin: x && x.role === Role.Admin,
        
        }));
    }

    componentDidUpdate() {
        if (this.state.mobileMenuActive)
            this.addClass(document.body, 'body-overflow-hidden');
        else
            this.removeClass(document.body, 'body-overflow-hidden');
    }

    render() {
        
        

        const { currentUser, isAdmin } = this.state;
        console.log(isAdmin);
        const wrapperClass = classNames('layout-wrapper', {
            'layout-overlay': this.state.layoutMode === 'overlay',
            'layout-static': this.state.layoutMode === 'static',
            'layout-static-sidebar-inactive': this.state.staticMenuInactive && this.state.layoutMode === 'static',
            'layout-overlay-sidebar-active': this.state.overlayMenuActive && this.state.layoutMode === 'overlay',
            'layout-mobile-sidebar-active': this.state.mobileMenuActive
        });

        const sidebarClassName = classNames("layout-sidebar", {
            'layout-sidebar-dark': this.state.layoutColorMode === 'dark',
            'layout-sidebar-light': this.state.layoutColorMode === 'light'
        });

        return (
        
            <Router history={history}>
               <div>
                    {currentUser &&
                        <div className={wrapperClass} onClick={this.onWrapperClick}>
                            <AppTopbar onToggleMenu={this.onToggleMenu} onLogout={this.logout} />
                            <div ref={(el) => this.sidebar = el} className={sidebarClassName} onClick={this.onSidebarClick}>
                            <div className="layout-logo">
                             <img alt="Logo" src={"assets/layout/images/logo-primary.svg"} />
                            </div>
                            <AppProfile />
                            <AppMenu model={this.menu} onMenuItemClick={this.onMenuItemClick} />
                            </div>
                            <div className="layout-main">
                            <Route path="/" exact component={Dashboard} />
                            <Route path="/Cycle"  exact component={Cycle} />
                            <Route path="/AddCycle" exact component={AddCycle}/>
                            <Route path="/Project" exact component={Project}/>
                            <Route path="/Task" exact component={Task}/>
                            <Route path="/Reports" exact component={Reports}/>
                            <Route path="/Calendar" exact component={Calendar}/>
                            <Route path="/Orgchart" exact component={Orgchart}/>
                            <Route path="/FileUploadDemo" exact component={FileUploadDemo}/>
                            <Route path="/JSONEditor" exact component={JSONEditor}/>
                            <Route path="/Scheduler" exact component={Scheduler}/>

                          </div>
                         
                        </div>
                    }
                    <div className="jumbotron">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-6 offset-md-3">
                                    
                                <PrivateRoute exact path="/" component={Dashboard} />
                                 <Route path="/login" component={LoginPage} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Router>
            
        );
    }
}

export default App;
