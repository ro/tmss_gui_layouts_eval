
import React, {Component} from 'react';
import 'primeicons/primeicons.css';
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.css';
import 'primeflex/primeflex.css';
import './layout/sass/AppTopbar.scss';
import {Menu} from 'primereact/menu';
import {Button} from 'primereact/button';
import { PropTypes } from 'prop-types';





 export class AppTopbar extends Component {

    constructor(props) {
        super(props);
        this.items = [
         {
             label: 'Welcome to Astron',
             
         },
         {
             label: 'PLC01 is in progress',
             
         }
     ];
 }
        
    
         static defaultProps = {
        
        
         onToggleMenu: null
    }
     
   
   static propTypes = {
       
    
        
        onToggleMenu: PropTypes.func.isRequired
    }
    

    render() {
      
        return (
            <div className="layout-topbar clearfix">
                    <button className="p-link layout-menu-button" onClick={this.props.onToggleMenu}>
                        <span className="pi pi-bars"/>
                    </button>
                     
                    <div className="layout-topbar-icons">
                    <span className="p-link">
                    <Menu model={this.items} popup={true} ref={el => this.menu = el} id="popup_menu"/>
                    <Button  icon="pi pi-bell" onClick={(event) => this.menu.toggle(event)} aria-controls="popup_menu" aria-haspopup={true}/>
                    </span>
                    <span></span>
                    
                    <button className="p-link">
                     <span className="layout-topbar-item-text">Settings</span>
                     <span className="layout-topbar-icon pi pi-cog"/>
                    </button>
                       <button className="p-link" onClick={this.props.onLogout}>
                        <span className="layout-topbar-item-text">Signout</span>
                        <span className="layout-topbar-icon pi pi-sign-out"/>
                       </button>
                       </div>
                      </div>
                
             
            
        
        )
        
      }
}
