import axios from 'axios';


export class NodeService {
    getTreeTableNodes() {
        return axios.get('assets/demo/data/treetablenodes.json')
                .then(res => res.data.root);
    }

}