# Astron TMSS GUI with PrimeReact
 
PrimeReact is a collection of rich UI components for React. All widgets are open source and free to use under MIT License.Enhanced mobile user experience with touch optimized responsive design elements.TMSS Layout is designed under the model of Sigma template.

  
-  [** View PrimeReact**](https://www.primefaces.org/sigma-react/#/documentation)

 # TMSS GUI ( Customized with PrimeReact)

**Customized layout design for TMSS POC**

**Dashboard**
![Alt text](./../reference/primereact/dashboard.png?raw=true  "Dashboard")
**Cycle**
![Alt text](./../reference/primereact/cycle.png?raw=true  "Cycle")
**Project**
![Alt text](./../reference/primereact/project.png?raw=true  "Project")
**Task**
![Alt text](./../reference/primereact/task.png?raw=true  "Task")
**Report**
![Alt text](./../reference/primereact/reports.png?raw=true  "Report")
**Calendar**
![Alt text](./../reference/primereact/calendar.png?raw=true  "Calendar")
**OrgChart**
![Alt text](./../reference/primereact/orgchart.png?raw=true  "OrgChart")
**FileUploadDemo**
![Alt text](./../reference/primereact/fileuploaddemo.png?raw=true  "FileUploadDemo")
 
**External components included into PrimeReact**

**Login form** 
![Alt text](./../reference/primereact/loginform.png?raw=true  "Login form")
**Scheduler**
![Alt text](./../reference/primereact/scheduler.png?raw=true  "Scheduler")
**JSONEditor**
![Alt text](./../reference/primereact/jsoneditor.png?raw=true  "JSONEditor")
    

# Installation
 
## Initial Configuration:

You need to have [NodeJs](https://nodejs.org/en/) (>= 10.0.0) installed on your local machine.
- Run git clone https://git.astron.nl/ro/tmss_gui_layouts_eval.git 
- Open tmss-primeface-react folder(cd tmss-primeface-react)
- Run `npm install`. 
- Run `npm start`. 
-  [URL - http://localhost:3000/](http://localhost:3000/)


User Credential 
- admin/admin
- astronomer/astro
- user/user
- operator/operator
- sos/sos