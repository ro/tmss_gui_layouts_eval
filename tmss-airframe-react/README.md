# Astron TMSS GUI with Airframe React
 

High Quality **Dashboard / Admin / Analytics template** that works great on any smartphone, tablet or desktop. Available as **Open Source as MIT license.**

  
-  [** View  Airframe React**](http://dashboards.webkom.co/react/airframe/)

 # TMSS GUI ( Customized Airframe React)

**Customized layout design for TMSS POC**

**Dashboard**
![Alt text](./../reference/airframe/dashboard.png?raw=true  "Dashboard")
**Cycles**
![Alt text](./../reference/airframe/cycle.png?raw=true  "Dashboard")
**Projects**
![Alt text](./../reference/airframe/projects.png?raw=true  "Dashboard")
**Task**
![Alt text](./../reference/airframe/task.png?raw=true  "Task")
**Report**
![Alt text](./../reference/airframe/reports.png?raw=true  "Report")
**Calendar**
![Alt text](./../reference/airframe/calendar.png?raw=true  "Calendar")
**File Upload**
![Alt text](./../reference/airframe/fileupload.png?raw=true  "File Upload")
 
**External components included into Airframe React**

**Login form** 
![Alt text](./../reference/airframe/loginform.png?raw=true  "Login form")
**Scheduler**
![Alt text](./../reference/airframe/scheduler.png?raw=true  "Scheduler")
**Advance View**
![Alt text](./../reference/airframe/advanceview.png?raw=true  "Advance View")
**Json Editor**
![Alt text](./../reference/airframe/Jsonschemaform.png?raw=true  "Json Editor")
    

# Installation
 
## Initial Configuration:

You need to have [NodeJs](https://nodejs.org/en/) (>= 10.0.0) installed on your local machine.
- Run git clone https://git.astron.nl/ro/tmss_gui_layouts_eval.git 
- Run `npm install`. 
- Run `npm start`. 
-  [URL - http://localhost:4100/](http://localhost:4100/)


User Credential 
- admin/admin
- dev/dev