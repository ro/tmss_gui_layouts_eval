import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import {
    NavItem,
    NavLink
} from './../../components';
import { authenticationService } from './../../_services/authentication.service';
const NavbarUser = (props) => (
    
    <NavItem { ...props }>
        <NavLink tag={ Link } to="/pages/login" onClick ={authenticationService.logout}>
            <i className="fa fa-power-off"></i>
        </NavLink>
    </NavItem>
);
NavbarUser.propTypes = {
    className: PropTypes.string,
    style: PropTypes.object
};

export { NavbarUser };
