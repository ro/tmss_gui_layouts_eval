import React from 'react';
import { hot } from 'react-hot-loader'
import { BrowserRouter as Router } from 'react-router-dom';

import AppLayout from './../../layout/default';
import { RoutedContent } from './../../routes';
// setup fake backend
import { configureFakeBackend } from './../../_helpers';

const basePath = process.env.BASE_PATH || '/';
   
const AppClient = () => {
    configureFakeBackend();
    return (
        <Router basename={ basePath }>
            <AppLayout>
                <RoutedContent />
            </AppLayout>
        </Router>
    );
}

export default hot(module)(AppClient);