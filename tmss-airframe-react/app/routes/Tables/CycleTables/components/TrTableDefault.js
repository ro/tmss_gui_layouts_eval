import React from 'react';
import PropTypes from 'prop-types';
import jdata from './../../../../data/cycletbl.json';
import { 
    UncontrolledButtonDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem
} from './../../../../components';

/*eslint-enable */
let tableData = jdata.data; 
var count = Object.keys(tableData).length;
const loadData = (index) =>{
     console.log('Table Data : '+this.tableData[index].name+'  - '+this.tableData[index].cycle);
}
const TrTableDefault = (props) => (
    
    <React.Fragment>
        {
            _.times(count, (index) => (
               
                <tr key={ index }>
                    <td className="align-middle">
                        <div className={ props.projectColor }>
                            {tableData[index].name}
                        </div>
                        <span>
                        {tableData[index].desc}
                        </span>
                    </td>
                    <td className="align-middle">
                        <div>
                        {tableData[index].start}
                        </div>
                        <span className="text-danger">
                        <i className="fa fa-circle-o text-success mr-2"></i>
                        {tableData[index].status}
                        </span>
                    </td>
                    <td className="align-middle">
                    <div>
                        {tableData[index].stop}
                        </div>
                    </td>
                    <td className="align-middle">
                        <div>
                        {tableData[index].stdhrs}
                         </div>   
                    </td>
                    <td className="align-middle">
                    <div>
                        {tableData[index].exphrs}
                         </div> 
                    </td>
                    <td className="align-middle">
                    <div>
                        {tableData[index].filhrs}
                         </div> 
                    </td>
                    <td className="align-middle">
                    <div>
                        {tableData[index].projects}
                         </div> 
                    </td>
                    <td className="align-middle text-right">
                        <UncontrolledButtonDropdown>
                            <DropdownToggle color="link" className={` text-decoration-none ${ props.dropdownColor } `}>
                                <i className="fa fa-gear"></i><i className="fa fa-angle-down ml-2"></i>
                            </DropdownToggle>
                            <DropdownMenu right >
                                <DropdownItem>
                                    <i className="fa fa-plus mr-2"></i>
                                    Add Project
                                </DropdownItem>
                                <DropdownItem>
                                    <i className="fa fa-pencil fa-fw mr-2"></i>
                                    Edit Cycle
                                </DropdownItem>
                                
                            </DropdownMenu>
                        </UncontrolledButtonDropdown>
                    </td>
                </tr>
            ))
        }
    </React.Fragment>
)

TrTableDefault.propTypes = {
    projectColor: PropTypes.node,
    leaderStatus: PropTypes.node,
    dropdownColor: PropTypes.node
};
TrTableDefault.defaultProps = {
    projectColor: "text-inverse",
    leaderStatus: "white",
    dropdownColor: ""
};

export { TrTableDefault };
