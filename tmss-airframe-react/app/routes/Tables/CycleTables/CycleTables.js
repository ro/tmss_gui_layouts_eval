import React from 'react';

import { 
    Container,
    Row,
    Col,
    Card,
    CardTitle,
    CardBody,
    Table,
} from '../../../components';

import { HeaderMain } from "../../components/HeaderMain";

import {
    TrTableDefault
} from "./components/TrTableDefault";

const Tables = () => (
    <React.Fragment>
        <Container>
            <HeaderMain 
                title="Cycle"
                className="mb-5 mt-4"
            />
            
            <Row>
                <Col lg={ 12 }>
                    <Card className="mb-3">
                        
                        { /* START Table */}
                        <Table className="mb-0" responsive>
                            <thead>
                                <tr>
                                    <th className="bt-0">Name</th>
                                    <th className="bt-0">Start</th>
                                    <th className="bt-0">Stop</th>
                                    <th className="bt-0">Standard Hrs</th>
                                    <th className="bt-0">Expert Hrs</th>
                                    <th className="bt-0">Filler Hrs</th>
                                    <th className="bt-0">Projects</th>
                                    <th className="text-right bt-0">
                                        Actions
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <TrTableDefault />
                            </tbody>
                        </Table>
                        { /* END Table */}
                    </Card>
                </Col>
            </Row>
            { /* END Section 1 */}

        </Container>
 
    </React.Fragment>
);

export default Tables;