import React from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import ToolkitProvider from 'react-bootstrap-table2-toolkit';
import moment from 'moment';
import _ from 'lodash';
import faker from 'faker/locale/en_US';

import {
    Avatar,
    Badge,
    Button,
    ButtonGroup,
    Row,
    Col
} from '../../../../components';
import { CustomExportCSV } from './CustomExportButton';
import { CustomSearch } from './CustomSearch';
import { randomArray, randomAvatar } from '../../../../utilities';

import axios from 'axios';
 
axios.defaults.baseURL = 'http://dummy.restapiexample.com/api/v1';
export class TableExample1 extends React.Component {
    constructor(props) {
        super(props) //since we are extending class Table so we have to use super in order to override Component class constructor
        this.state = { //state is by default an object
            students: [
                { "id": 1, "name": 'SCH-01', "cycle": "LC1", "project": 'Proj-01' , "unit":"2", "state":"Finished"},
                { "id": 2, "name": 'SCH-02', "cycle": "LC2", "project": 'Proj-02', "unit":"2", "state":"Approved" },
                { "id": 3, "name": 'SCH-03', "cycle": "LC1", "project": 'Proj-02', "unit":"2", "state":"In Progress" },
                { "id": 4, "name": 'SCH-04', "cycle": "LC1", "project": 'Proj-03' , "unit":"2", "state":"Defined"}
             ]
        }
     }
  
       renderTableData() {
          return this.state.students.map((student, index) => {
             let col = Object.keys(student)
             return (
                <tr key={student.id}>
                   {col.map((val, index) => {
                      return <td key={index}>{student[col[index]]}</td>
                   })}
                </tr>
             )
          })
       }
    
        

       renderTableHeader() {
          let header = Object.keys(this.state.students[0])
          return header.map((key, index) => {
             return <th key={index}>{key.toUpperCase()}</th>
          })
       }
    
       render() {
          return (
             <div>
                
                <table id='students'>
                   <tbody>
                      <tr>{this.renderTableHeader()}</tr>
                      {this.renderTableData()}
                   </tbody>
                </table>
             </div>
          )
       }
}