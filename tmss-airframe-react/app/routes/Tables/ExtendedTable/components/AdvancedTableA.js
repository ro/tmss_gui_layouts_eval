import React from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import filterFactory, { Comparator, dateFilter } from 'react-bootstrap-table2-filter'
import ToolkitProvider from 'react-bootstrap-table2-toolkit';
import _ from 'lodash';
import faker from 'faker/locale/en_US';
import moment from 'moment';

import {
    Badge,
    Button,
    CustomInput,
    StarRating,
    ButtonGroup,
    Container,
    Row,
    Col,
    Card,
    CardTitle,
    CardBody,
    InputGroup,
    InputGroupAddon,
    Form, 
    FormGroup, 
    Label, 
    Input, 
    FormText,
    UncontrolledButtonDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
     
    UncontrolledModal,
    ModalHeader,
    CardGroup,
    ModalBody,
    ModalFooter
} from './../../../../components';
import { CustomExportCSV } from './CustomExportButton';
import { CustomSearch } from './CustomSearch';
import { CustomPaginationPanel } from './CustomPaginationPanel';
import { CustomSizePerPageButton } from './CustomSizePerPageButton';
import { CustomPaginationTotal } from './CustomPaginationTotal';
import { randomArray,checkRolePermisson } from './../../../../utilities';
import {
    buildCustomTextFilter,
    buildCustomSelectFilter,
    buildCustomNumberFilter
} from './../filters';

 
const INITIAL_PRODUCTS_COUNT = 500;

const ProductQuality = {
    Good: 'product-quality__good',
    Bad: 'product-quality__bad',
    Unknown: 'product-quality__unknown'
};

const namelist = ["Pro-1", "pro2", "pro12", "pro4", "pro5", "pro6", "pro12", "pro12", "pro12", "pro12", "pro12"];
const sortCaret = (order) => {
    if (!order)
        return <i className="fa fa-fw fa-sort text-muted"></i>;
    if (order)
        return <i className={`fa fa-fw text-muted fa-sort-${order}`}></i>
}

const generateRow = (index) => ({
    id: randomArray(['Proj-01','Proj-02','Proj-03','Proj-04','Proj-05' ]), 
    name:  randomArray(['5','3','6','9']),
    quality: randomArray(['Tag4 Tag1','Tag2 Tag1','Tag3 Tag2','Tag3 Tag1']),
    price: randomArray([
        ProductQuality.Good,
        ProductQuality.Unknown
    ]),
    satisfaction: randomArray([
        ProductQuality.Good,
        ProductQuality.Unknown
    ]),
    inStockDate: randomArray(['true','false'])
});

export class AdvancedTableA extends React.Component {
    constructor() {
        super();
        
        this.state = {
            products: _.times(5, generateRow),
            selected: []
        };

        this.headerCheckboxRef = React.createRef();
    }

    handleSelect(row, isSelected) {
        if (isSelected) {
            this.setState({ selected: [...this.state.selected, row.id] })
        } else {
            this.setState({
                selected: this.state.selected.filter(itemId => itemId !== row.id)
            })
        }
    }

    handleSelectAll(isSelected, rows) {
        if (isSelected) {
            this.setState({ selected: _.map(rows, 'id') })
        } else {
            this.setState({ selected: [] });
        }
    }

    handleAddRow() {
        const currentSize = this.state.products.length;

        this.setState({
            products: [
                generateRow(currentSize + 1),
                ...this.state.products,
            ]
        });
    }

    handleDeleteRow() {
        this.setState({
            products: _.filter(this.state.products, product =>
                !_.includes(this.state.selected, product.id))
        })
    }

    handleResetFilters() {
        this.nameFilter('');
        this.qualityFilter('');
        this.priceFilter('');
        this.satisfactionFilter('');
    }

    createColumnDefinitions() {
        return [{
            dataField: 'id',
            text: 'Name',
            sort: true,
            size: 'lg',
            sortCaret
        }, {
            dataField: 'name',
            text: 'Priority',
            sort: true,
            headerStyle: {width: '200px'},
            sortCaret
             
        }, {
            dataField: 'quality',
            text: 'Tags',
            headerStyle: {width: '200px'},
            formatter: (cell) => {
                const color = (status) => {
                    const map = { 
                    'Tag4 Tag1': 'info',
                    'Tag2 Tag1': 'info',
                    'Tag3 Tag2': 'info',
                    'Tag3 Tag1': 'info'
                    };
                    return map[status] ;
                }

                return (
                    <Badge color={ color(cell) }>
                        { cell }
                    </Badge>
                );
            },
            sort: true,
            sortCaret
        }, {
            dataField: 'price',
            text: 'Private',
            headerStyle: {width: '200px'},
            formatter: (cell) => {
                let pqProps;
                switch (cell) {
                    case ProductQuality.Good:
                        pqProps = {
                            color: 'success',
                            text: <i class="fa fa-check" aria-hidden="true"></i>
                        }
                    break;
                    case ProductQuality.Unknown:
                    default:
                        pqProps = {
                            color: 'danger',
                            text: <i class="fa fa-times" aria-hidden="true"></i>
                        }
                }

                return (
                    <Badge color={pqProps.color}>
                        { pqProps.text }
                    </Badge>
                )
            },
            sort: true,
            sortCaret 
        }, {
            dataField: 'satisfaction',
            text: 'Expert',
            headerStyle: {width: '200px'},
            formatter: (cell) => {
                let pqProps;
                switch (cell) {
                    case ProductQuality.Good:
                        pqProps = {
                            color: 'success',
                            text: <i class="fa fa-check" aria-hidden="true"></i>
                        }
                    break;
                    case ProductQuality.Unknown:
                    default:
                        pqProps = {
                            color: 'danger',
                            text: <i class="fa fa-times" aria-hidden="true"></i>
                        }
                }

                return (
                    <Badge color={pqProps.color}>
                        { pqProps.text }
                    </Badge>
                )
            },
            sort: true,
            sortCaret 
        }, {
            dataField: 'price',
            text: 'Filler', 
            headerStyle: {width: '200px'},
            formatter: (cell) => {
                let pqProps;
                switch (cell) {
                    case ProductQuality.Good:
                        pqProps = {
                            color: 'success',
                            text: <i class="fa fa-check" aria-hidden="true"></i>
                        }
                    break;
                    case ProductQuality.Unknown:
                    default:
                        pqProps = {
                            color: 'danger',
                            text: <i class="fa fa-times" aria-hidden="true"></i>
                        }
                }

                return (
                    <Badge color={pqProps.color}>
                        { pqProps.text }
                    </Badge>
                )
            },
            sort: true,
            sortCaret
        }]; 
    }

    render() {
        const columnDefs = this.createColumnDefinitions();
        const paginationDef = paginationFactory({
            paginationSize: 5,
            showTotal: true,
            pageListRenderer: (props) => (
                <CustomPaginationPanel { ...props } size="sm" className="ml-md-auto mt-2 mt-md-0" />
            ),
            sizePerPageRenderer: (props) => (
                <CustomSizePerPageButton { ...props } />
            ),
            paginationTotalRenderer: (from, to, size) => (
                <CustomPaginationTotal { ...{ from, to, size } } />
            )
        });
        const selectRowConfig = {
            mode: 'checkbox',
            selected: this.state.selected,
            onSelect: this.handleSelect.bind(this),
            onSelectAll: this.handleSelectAll.bind(this),
            selectionRenderer: ({ mode, checked, disabled }) => (
                <CustomInput type={ mode } checked={ checked } disabled={ disabled } />
            ),
            selectionHeaderRenderer: ({ mode, checked, indeterminate }) => (
                <CustomInput type={ mode } checked={ checked } innerRef={el => el && (el.indeterminate = indeterminate)} />
            )
        };

        return (
            <ToolkitProvider
                keyField="id"
                data={ this.state.products }
                columns={ columnDefs }
                search
                exportCSV
            >
            {
                props => (
                    <React.Fragment>
                        <div className="d-flex justify-content-end align-items-center mb-2">
                            <h6 className="my-0">
                            Project Table -1
                            </h6>
                            <div className="d-flex ml-auto">
                                <CustomSearch
                                    className="mr-2"
                                    { ...props.searchProps }
                                />

                                <ButtonGroup >
                                    <CustomExportCSV
                                        { ...props.csvProps }
                                    >
                                       <i class="fa fa-download" aria-hidden="true" style={{color:'green'}}></i>
                                    </CustomExportCSV>
                                    { checkRolePermisson('Project', 'delete') &&
                                        <Button
                                            size="sm"
                                            outline
                                            onClick={ this.handleDeleteRow.bind(this) }
                                        >
                                        <i class="fa fa-trash-o" style={{color:'red'}}></i>
                                        </Button>
                                    }
                                    { checkRolePermisson('Project', 'add') &&
                                        <Button id="addproject1"
                                            size="sm"
                                            outline
                                            
                                        >
                                            <i className="fa fa-fw fa-plus" style={{color:'green'}}></i>
                                        </Button>
                                    }
                                     
                                </ButtonGroup>

                                <UncontrolledModal target="addproject1" >
                                   
                                    <ModalBody >
                                        <div style={{float: 'left'}}>
                                            <div >
                                             <h3> Add Project</h3> 
                                            </div>
                                       

                                        <Form>
                                { /* START Input */}
                                <FormGroup row>
                                    <Label for="input-1" sm={4}>
                                        Name
                                    </Label>
                                    <Col sm={8}>
                                        <Input 
                                            type="text" 
                                            name="" 
                                            id="input-1" 
                                            placeholder="Enter Project Name..." 
                                        />
                                    </Col>
                                </FormGroup>
                                { /* END Input */}
                                { /* START Input */}
                                <FormGroup row>
                                    <Label for="inputPassword-1" sm={4}>
                                        Priority
                                    </Label>
                                    <Col sm={8}>
                                        <Input 
                                            type="number" 
                                            name="" 
                                            id="inputPassword-1" 
                                            placeholder="Priority" 
                                        />
                                    </Col>
                                </FormGroup>
                                { /* END Input */}
                                { /* START Select */}
                                <FormGroup row>
                                    <Label for="country-selector-1" sm={4}>
                                        Country
                                    </Label>
                                    <Col sm={8}>
                                        <CustomInput 
                                            type="select" 
                                            name="customSelect" 
                                            id="country-selector-1"  
                                        >
                                            <option>Tag1</option>
                                            <option>Tag2</option>
                                            <option>Tag3</option>
                                            <option>Tag4</option>
                                            <option>Tag5</option>
                                        </CustomInput>
                                    </Col>
                                </FormGroup>

                                <FormGroup row>
                                    <Label for="operatingSystem11" sm={4} className="pt-0">
                                        Private
                                    </Label>
                                    <Col sm={8}>
                                        <CustomInput 
                                            type="radio" 
                                            id="operatingSystem11"
                                            name="operatingSystem1"
                                            label="True" 
                                            inline
                                            defaultChecked
                                        />
                                        <CustomInput 
                                            type="radio" 
                                            id="operatingSystem12"
                                            name="operatingSystem1"
                                            label="False" 
                                            inline
                                        />
                                        
                                    </Col>
                                </FormGroup>

                                <FormGroup row>
                                    <Label for="operatingSystem21" sm={4} className="pt-0">
                                        Expert
                                    </Label>
                                    <Col sm={8}>
                                        <CustomInput 
                                            type="radio" 
                                            id="operatingSystem21"
                                            name="operatingSystem2"
                                            label="True" 
                                            inline
                                            defaultChecked
                                        />
                                        <CustomInput 
                                            type="radio" 
                                            id="operatingSystem22"
                                            name="operatingSystem2"
                                            label="False" 
                                            inline
                                        />
                                        
                                    </Col>
                                </FormGroup>
                                <FormGroup row>
                                    <Label for="operatingSystem31" sm={4} className="pt-0">
                                        Filler
                                    </Label>
                                    <Col sm={8}>
                                        <CustomInput 
                                            type="radio" 
                                            id="operatingSystem31"
                                            name="operatingSystem3"
                                            label="True" 
                                            inline
                                            defaultChecked
                                        />
                                        <CustomInput 
                                            type="radio" 
                                            id="operatingSystem32"
                                            name="operatingSystem3"
                                            label="False" 
                                            inline
                                        />
                                        
                                    </Col>
                                </FormGroup>
                                { /* END Select */}
                                { /* START File Select */}
                                <FormGroup row>
                                    <Label for="addCv" sm={4}>
                                        Upload Documents
                                    </Label>
                                    <Col sm={8}>
                                        <CustomInput type="file" id="addCv" name="customFile" label="Choose file..." />
                                        <FormText color="muted">
                                            Accepted formats: pdf, doc, txt. Max file size 7Mb
                                        </FormText>
                                    </Col>
                                </FormGroup>
                                { /* END File Select */}
                                { /* START Textarea */}
                                <FormGroup row>
                                    <Label for="message-1" sm={4}>
                                        Description
                                    </Label>
                                    <Col sm={8}>
                                        <Input 
                                            type="textarea" 
                                            name="text" 
                                            id="message-1" 
                                            placeholder="Enter project description..." 
                                            className="mb-2"
                                        />
                                         
                                       
                                    </Col>
                                </FormGroup>
                                { /* END Textarea */}
                            </Form>
     
                                        </div>
                                    </ModalBody>
                                    <ModalFooter>
                                        <UncontrolledModal.Close   color="secondary">
                                            Close
                                        </UncontrolledModal.Close>
                                        <Button color="info">Save</Button>
                                    </ModalFooter>
                                </UncontrolledModal>
                            </div>
                        </div>
                        <BootstrapTable
                        size="lg"
                            classes="table-responsive"
                            pagination={ paginationDef }
                            filter={ filterFactory() }
                            selectRow={ selectRowConfig }
                            bordered={ false }
                            responsive
                            { ...props.baseProps }
                        />
                    </React.Fragment>
                )
            }
            </ToolkitProvider>
        );
    }
}