import React from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import ToolkitProvider from 'react-bootstrap-table2-toolkit';
import moment from 'moment';
import _ from 'lodash';
import faker from 'faker/locale/en_US';

import {
    Badge,
    Button,
    CustomInput,
    StarRating,
    ButtonGroup,
    Container,
    Row,
    Col,
    Card,
    CardTitle,
    CardBody,
    InputGroup,
    InputGroupAddon,
    Form, 
    FormGroup, 
    Label, 
    Input, 
    FormText,
    UncontrolledButtonDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
     
    UncontrolledModal,
    ModalHeader,
    CardGroup,
    ModalBody,
    ModalFooter
} from './../../../../components';
import { CustomExportCSV } from './CustomExportButton';
import { CustomSearch } from './CustomSearch';
import { randomArray, randomAvatar, checkRolePermisson } from './../../../../utilities';
 

const ProductQuality = {
    Good: 'product-quality__good',
    Bad: 'product-quality__bad',
    Unknown: 'product-quality__unknown'
};

const namelist = ["Pro-1", "Pro-2", "Pro-3", "Pro-4", "Pro-5", "Pro-6", "Pro-7", "Pro-8", "Pro-9", "Pro-10", "Pro-11"];

const generateRow = (id) => ({
    id,
    photo: randomAvatar(),
    firstName: namelist[id],
    lastName: randomArray(['5','3','6','9']),
    role:  randomArray([
        ProductQuality.Good,
        ProductQuality.Unknown
    ]),
    status: randomArray([
        'Tag-1',
        'Tag-2',
        'Tag-3',
        'Tag-4',
        'Tag-5',
        'Tag-6'
        
    ]),
    region: randomArray([
        ProductQuality.Good,
        ProductQuality.Unknown
    ]),
    earnings: randomArray([
        ProductQuality.Good,
        ProductQuality.Unknown
    ]),
    earningsCurrencyIcon: randomArray([
        <i className="fa fa-fw fa-euro text-muted" key="cur_eur"></i>,
        <i className="fa fa-fw fa-dollar text-muted" key="cur_usd"></i>
    ]),
    lastLoginDate: faker.date.recent(),
    ipAddress: faker.internet.ip(),
    browser: 'Safari 9.1.1(11601.6.17)',
    os: 'OS X El Capitan',
    planSelected: randomArray(['Basic', 'Premium', 'Enterprise']),
    planEnd: faker.date.future()
});

const sortCaret = (order) => {
    if (!order)
        return <i className="fa fa-fw fa-sort text-muted"></i>;
    if (order)
        return <i className={`fa fa-fw text-muted fa-sort-${order}`}></i>
};

export class AdvancedTableB extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            users: _.times(10, generateRow)
        }
    }

    handleAddRow() {
        const usersLength = this.state.users.length;

        this.setState({
            users: [
                generateRow(usersLength + 1),
                ...this.state.users
            ]
        })
    }

    createColumnDefinitions() {
        return [
              {
                dataField: 'firstName',
                text: 'Name',
                sort: true,
                sortCaret
            }, {
                dataField: 'lastName',
                text: 'Priority',
                sort: true,
                sortCaret
            },  {
                dataField: 'status',
                text: 'Tags',
                sort: true,
                sortCaret,
                formatter: (cell) => {
                    const color = (status) => {
                        const map = {
                            'Active': 'success',
                            'Suspended': 'danger',
                            'Waiting': 'info',
                            'Unknown': 'secondary'
                        };
                        return map[status];
                    }

                    return (
                        <Badge color={ color(cell) }>
                            { cell }
                        </Badge>
                    );
                }
            }, {
                dataField: 'region',
                text: 'Private',
                headerStyle: {width: '200px'},
                formatter: (cell) => {
                    let pqProps;
                    switch (cell) {
                        case ProductQuality.Good:
                            pqProps = {
                                color: 'success',
                                text: <i class="fa fa-check" aria-hidden="true"></i>
                            }
                        break;
                        case ProductQuality.Unknown:
                        default:
                            pqProps = {
                                color: 'danger',
                                text: <i class="fa fa-times" aria-hidden="true"></i>
                            }
                    }
    
                    return (
                        <Badge color={pqProps.color}>
                            { pqProps.text }
                        </Badge>
                    )
                },
                sort: true,
                sortCaret 
            }, {
                dataField: 'earnings',
                text: 'Expert',
                formatter: (cell) => {
                    let pqProps;
                    switch (cell) {
                        case ProductQuality.Good:
                            pqProps = {
                                color: 'success',
                                text: <i class="fa fa-check" aria-hidden="true"></i>
                            }
                        break;
                        case ProductQuality.Unknown:
                        default:
                            pqProps = {
                                color: 'danger',
                                text: <i class="fa fa-times" aria-hidden="true"></i>
                            }
                    }
    
                    return (
                        <Badge color={pqProps.color}>
                            { pqProps.text }
                        </Badge>
                    )
                },
                sort: true,
                sortCaret 
            },
            {
                dataField: 'role',
                text: 'Filler',
                formatter: (cell) => {
                    let pqProps;
                    switch (cell) {
                        case ProductQuality.Good:
                            pqProps = {
                                color: 'success',
                                text: <i class="fa fa-check" aria-hidden="true"></i>
                            }
                        break;
                        case ProductQuality.Unknown:
                        default:
                            pqProps = {
                                color: 'danger',
                                text: <i class="fa fa-times" aria-hidden="true"></i>
                            }
                    }
    
                    return (
                        <Badge color={pqProps.color}>
                            { pqProps.text }
                        </Badge>
                    )
                },
                sort: true,
                sortCaret
            },
             
        ]; 
    }

    render() {
        const columnDefs = this.createColumnDefinitions();

        const expandRow = {
            renderer: row => (
                <Row>
                    <Col md={ 6 }>
                        <dl className="row">
                            <dt className="col-sm-6 text-right">Last Login</dt>
                            <dd className="col-sm-6">{ moment(row.lastLoginDate).format('DD-MMM-YYYY') }</dd>

                            <dt className="col-sm-6 text-right">IP Address</dt>
                            <dd className="col-sm-6">{ row.ipAddress }</dd>

                            <dt className="col-sm-6 text-right">Browser</dt>
                            <dd className="col-sm-6">{ row.browser }</dd>
                        </dl>
                    </Col>
                    <Col md={ 6 }>
                        <dl className="row">
                            <dt className="col-sm-6 text-right">Operating System</dt>
                            <dd className="col-sm-6">{ row.os }</dd>

                            <dt className="col-sm-6 text-right">Selected Plan</dt>
                            <dd className="col-sm-6">{ row.planSelected }</dd>

                            <dt className="col-sm-6 text-right">Plan Expiriation</dt>
                            <dd className="col-sm-6">{ moment(row.planEnd).format('DD-MMM-YYYY') }</dd>
                        </dl>
                    </Col>
                </Row>
            ),
            showExpandColumn: true,
            expandHeaderColumnRenderer: ({ isAnyExpands }) => isAnyExpands ? (
                    <i className="fa fa-angle-down fa-fw fa-lg text-muted"></i>
                ) : (
                    <i className="fa fa-angle-right fa-fw fa-lg text-muted"></i>
                ),
            expandColumnRenderer: ({ expanded }) =>
                expanded ? (
                    <i className="fa fa-angle-down fa-fw fa-lg text-muted"></i>
                ) : (
                    <i className="fa fa-angle-right fa-fw fa-lg text-muted"></i>
                )
        }

        return (
            <ToolkitProvider
                keyField="id"
                data={ this.state.users }
                columns={ columnDefs }
                search
                exportCSV
            >
            {
                props => (
                    <React.Fragment>
                        <div className="d-flex justify-content-end align-items-center mb-2">
                            <h6 className="my-0">
                            Projects
                            </h6>
                            <div className="d-flex ml-auto">
                                <CustomSearch
                                    className="mr-2"
                                    { ...props.searchProps }
                                />
                                <ButtonGroup>
                                <CustomExportCSV
                                        { ...props.csvProps }
                                    >
                                       <i class="fa fa-download" aria-hidden="true" style={{color:'green'}}></i>
                                    </CustomExportCSV>
                                    { checkRolePermisson('Projects', 'add') &&
                                        <Button id="addproject1"
                                            size="sm"
                                            outline
                                        >
                                            <i className="fa fa-fw fa-plus" style={{color:'green'}}></i>
                                        </Button>
                                    }
                                </ButtonGroup>
                                <UncontrolledModal target="addproject1" >
                                    <ModalBody >
                                        <div style={{float: 'left'}}>
                                            <div >
                                             <h3> Add Project</h3> 
                                            </div>
                                        <Form>
                                { /* START Input */}
                                <FormGroup row>
                                    <Label for="input-1" sm={4}>
                                        Name
                                    </Label>
                                    <Col sm={8}>
                                        <Input 
                                            type="text" 
                                            name="" 
                                            id="input-1" 
                                            placeholder="Enter project name..." 
                                        />
                                    </Col>
                                </FormGroup>
                                { /* END Input */}
                                { /* START Input */}
                                <FormGroup row>
                                    <Label for="inputPassword-1" sm={4}>
                                        Priority
                                    </Label>
                                    <Col sm={8}>
                                        <Input 
                                            type="number" 
                                            name="" 
                                            id="inputPassword-1" 
                                            placeholder="Priority" 
                                        />
                                    </Col>
                                </FormGroup>
                                { /* END Input */}
                                { /* START Select */}
                                <FormGroup row>
                                    <Label for="country-selector-1" sm={4}>
                                        Tag
                                    </Label>
                                    <Col sm={8}>
                                        <CustomInput 
                                            type="select" 
                                            name="customSelect" 
                                            id="country-selector-1"  
                                        >
                                            <option>Tag1</option>
                                            <option>Tag2</option>
                                            <option>Tag3</option>
                                            <option>Tag4</option>
                                            <option>Tag5</option>
                                        </CustomInput>
                                    </Col>
                                </FormGroup>

                                <FormGroup row>
                                    <Label for="operatingSystem11" sm={4} className="pt-0">
                                        Private
                                    </Label>
                                    <Col sm={8}>
                                        <CustomInput 
                                            type="radio" 
                                            id="operatingSystem11"
                                            name="operatingSystem1"
                                            label="True" 
                                            inline
                                            defaultChecked
                                        />
                                        <CustomInput 
                                            type="radio" 
                                            id="operatingSystem12"
                                            name="operatingSystem1"
                                            label="False" 
                                            inline
                                        />
                                        
                                    </Col>
                                </FormGroup>

                                <FormGroup row>
                                    <Label for="operatingSystem21" sm={4} className="pt-0">
                                        Expert
                                    </Label>
                                    <Col sm={8}>
                                        <CustomInput 
                                            type="radio" 
                                            id="operatingSystem21"
                                            name="operatingSystem2"
                                            label="True" 
                                            inline
                                            defaultChecked
                                        />
                                        <CustomInput 
                                            type="radio" 
                                            id="operatingSystem22"
                                            name="operatingSystem2"
                                            label="False" 
                                            inline
                                        />
                                        
                                    </Col>
                                </FormGroup>
                                <FormGroup row>
                                    <Label for="operatingSystem31" sm={4} className="pt-0">
                                        Filler
                                    </Label>
                                    <Col sm={8}>
                                        <CustomInput 
                                            type="radio" 
                                            id="operatingSystem31"
                                            name="operatingSystem3"
                                            label="True" 
                                            inline
                                            defaultChecked
                                        />
                                        <CustomInput 
                                            type="radio" 
                                            id="operatingSystem32"
                                            name="operatingSystem3"
                                            label="False" 
                                            inline
                                        />
                                        
                                    </Col>
                                </FormGroup>
                                { /* END Select */}
                                { /* START File Select */}
                                <FormGroup row>
                                    <Label for="addCv" sm={4}>
                                        Upload Documents
                                    </Label>
                                    <Col sm={8}>
                                        <CustomInput type="file" id="addCv" name="customFile" label="Choose file..." />
                                        <FormText color="muted">
                                            Accepted formats: pdf, doc, txt. Max file size 7Mb
                                        </FormText>
                                    </Col>
                                </FormGroup>
                                { /* END File Select */}
                                { /* START Textarea */}
                                <FormGroup row>
                                    <Label for="message-1" sm={4}>
                                        Description
                                    </Label>
                                    <Col sm={8}>
                                        <Input 
                                            type="textarea" 
                                            name="text" 
                                            id="message-1" 
                                            placeholder="Enter project description..." 
                                            className="mb-2"
                                        />
                                         
                                       
                                    </Col>
                                </FormGroup>
                                { /* END Textarea */}
                            </Form>
     
                                        </div>
                                    </ModalBody>
                                    <ModalFooter>
                                        <UncontrolledModal.Close   color="secondary">
                                            Close
                                        </UncontrolledModal.Close>
                                        <Button color="info">Save</Button>
                                    </ModalFooter>
                                </UncontrolledModal>
                            </div>
                        </div>
                        <BootstrapTable
                            classes="table-responsive-lg"
                            bordered={ false }
                            expandRow={ expandRow }
                            responsive
                            hover
                            { ...props.baseProps }
                        />
                    </React.Fragment>
                )
            }
            </ToolkitProvider>
        );
    }
}