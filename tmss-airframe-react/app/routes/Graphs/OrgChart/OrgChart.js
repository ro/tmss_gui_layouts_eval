import React, {Component} from 'react';
import {OrganizationChart} from './components/OrganizationChartNode';

import './../../../styles/components/orgchart.scss';

class OrgChart extends Component {

    constructor() {
        super();
        this.state = {
            selection: []
        };

        this.data1 = [{
            label: 'Project-1',
            type: 'person',
            className: 'p-person',
            expanded: true,
            data: {name:'Project -1 details', 'avatar': 'walter.jpg'},
            children: [
                {
                    label: 'Schedule-1',
                    type: 'person',
                    className: 'p-person',
                    expanded: true,
                    data: {name:'Schedule-1', 'avatar': 'saul.jpg'},
                    children:[{
                        label: 'S1-Task-1',
                        className: 'department-cfo'
                    },
                    {
                        label: 'S1-Task-2',
                        className: 'department-cfo'
                    }],
                },
                {
                    label: 'Schedule-2',
                    type: 'person',
                    className: 'p-person',
                    expanded: true,
                    data: {name:'Schedule-2', 'avatar': 'mike.jpg'},
                    children:[{
                        label: 'S2-Task-1',
                        className: 'department-coo'
                    }]
                },
                {
                    label: 'Schedule-3',
                    type: 'person',
                    className: 'p-person',
                    expanded: true,
                    data: {name:'Schedule-3', 'avatar': 'jesse.jpg'},
                    children:[{
                        label: 'S3-Task-1',
                        className: 'department-cto',
                        expanded: true,
                        children:[{
                            label: 'S3-Task-1-Sub-1',
                            className: 'department-cto'
                        },
                        {
                            label: 'S3-Task-1-Sub-2',
                            className: 'department-cto'
                        },
                        {
                            label: 'S3-Task-1-Sub-3',
                            className: 'department-cto'
                        }]
                    },
                    {
                        label: 'S3-Task-2',
                        className: 'department-cto'
                    },
                    {
                        label: 'S3-Task-3',
                        className: 'department-cto'
                    }]
                },
            ]
        }];

        this.nodeTemplate = this.nodeTemplate.bind(this);
    }

    nodeTemplate(node) {
        if (node.type === "person") {
            return (
                <div>
                    <div className="node-header">{node.label}</div>
                    <div className="node-content">
                        <img alt={node.data.avatar} src={`showcase/demo/images/organization/${node.data.avatar}`} srcSet="https://www.primefaces.org/wp-content/uploads/2020/05/placeholder.png" style={{ width: '32px' }}/>
                        <div>{node.data.name}</div>
                    </div>
                </div>
            );
        }

        if (node.type === "department") {
            return node.label;
        }
    }

    render() {
        return (
            <div className="organizationchart-demo">
                <h3>Advanced View</h3>
                <OrganizationChart value={this.data1} nodeTemplate={this.nodeTemplate} selection={this.state.selection} selectionMode="multiple"
                    onSelectionChange={event => this.setState({selection: event.data})} className="company"></OrganizationChart>

               
            </div>
        )
    }
}

export default OrgChart;