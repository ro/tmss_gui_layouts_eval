import React from 'react';

import {
    Container,
    Row,
    Col,
    Card,
    CardBody,
    CardDeck, 
    Button
} from './../../../components'
import { HeaderMain } from "../../components/HeaderMain";
import { HeaderDemo } from "../../components/HeaderDemo";

import { SimpleBarChart } from "./components/SimpleBarChart";
import { StackedBarChart } from "./components/StackedBarChart";
import { MixBarChart } from "./components/MixBarChart";
import { PositiveAndNegativeBarChart } from "./components/PositiveAndNegativeBarChart";
import { BarChartStackedBySign } from "./components/BarChartStackedBySign";
import { BarChartHasBackground } from "./components/BarChartHasBackground";
import { SimpleLineChart } from "./components/SimpleLineChart";
import { DashedLineChart } from "./components/DashedLineChart";
import { VerticalLineChart } from "./components/VerticalLineChart";
import { CustomizedLabelLineChart } from './components/CustomizedLabelLineChart';
import { SimpleAreaChart } from "./components/SimpleAreaChart";
import { StackedAreaChart } from "./components/StackedAreaChart";
import { PercentAreaChart } from "./components/PercentAreaChart";
import { AreaChartFillByValue } from "./components/AreaChartFillByValue";
import { TwoLevelPieChart } from "./components/TwoLevelPieChart";
import { StraightAnglePieChart } from "./components/StraightAnglePieChart";
import { PieChartWithCustomizedLabel } from "./components/PieChartWithCustomizedLabel";
import { PieChartWithPaddingAngle } from "./components/PieChartWithPaddingAngle";
import { PieChartWithPaddingAngleHalf } from "./components/PieChartWithPaddingAngleHalf";
import { SpecifiedDomainRadarChart } from "./components/SpecifiedDomainRadarChart";
import { SimpleRadialBarChart } from './components/SimpleRadialBarChart';
import { LineBarAreaComposedChart } from "./components/LineBarAreaComposedChart";
import { TinyLineChart } from "./components/TinyLineChart";
import { TinyAreaChart } from "./components/TinyAreaChart";
import { TinyBarChart } from './components/TinyBarChart';
import { TinyPieChart } from './components/TinyPieChart';
import { TinyDonutChart } from './components/TinyDonutChart';
import { VerticalComposedChart } from './components/VerticalComposedChart';

import {
    AdvancedTableA,
    AdvancedTableB,
    BasicTable,
    BorderedTable,
    CellEdit,
    ClearSearch,
    LargeTable,
    SortTable,
    TableExample1,
    TableExample
} from './../../Tables/ExtendedTable/components';

export const ChartsExample = () => (
    <Container>
        
        <CardDeck>
            { /* START Card Graph */}
            <Card className="mb-6">
                <CardBody>
                    <div className="d-flex">
                        <div>
                            
                            <p>Bar Charts</p>
                        </div>
                        <span className="ml-auto">
                             
                        </span>
                    </div>
                    <SimpleBarChart />
                </CardBody>
            </Card>
            { /* START Card Graph */}
            { /* START Card Graph */}
            <Card className="mb-3">
                <CardBody>
                    <div className="d-flex">
                        <div>
                            
                            <p>Schedule Tasks</p>
                        </div>
                        <span className="ml-auto">
                             
                        </span>
                    </div>
                    
                   <div >
                   <TableExample />
                   {/* <AdvancedTableB /> */}
                   </div>
                </CardBody>
            </Card>
            { /* START Card Graph */}
        </CardDeck>
         
    </Container>
);

export default ChartsExample;
