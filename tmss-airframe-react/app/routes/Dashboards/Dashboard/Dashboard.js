import React from 'react';
import faker from 'faker/locale/en_US';
import {
    Container,
    Row,
    Table,
    Col
} from '../../../components';

import { HeaderMain } from "../../components/HeaderMain";
import {
    CardSystem

} from "./components/CardSystem";
 
import {
    TrSystem
} from "./components/trSystem"

import  ChartsExample  from "./../../Graphs/ChartsExample";

const TrColors =  [
        {
            fill: "primary-02",
            stroke: "primary"
        },
        {
            fill: "purple-02",
            stroke: "purple"
        },
        {
            fill: "success-02",
            stroke: "success"
        },
        {
            fill: "yellow-02",
            stroke: "yellow"
        }
    ]
 
const Dashboard = () => (
    <Container>
        <Row className="mb-5">
            <Col lg={ 12 }>
                <HeaderMain 
                    title="Dashboard"
                />
            </Col>
            <Col lg={ 3 } md={ 3 }>
               <CardSystem
                    title="Active Projects"
                    badgeColor="#89e980"
                    pieColor="black"
                    badgeColor="#87c2e9"
                    value="50"
                    icon="fa fa-fw fa-th"
               />
            </Col>
            <Col lg={ 3 } md={ 6 }>
               <CardSystem
                    title="Active Schedules"
                    unit="Mb"
                    badgeColor="purple"
                    pieColor="black"
                    badgeColor="#89e980"
                    value="100"
                    icon="fa fa-calendar"
               />
            </Col>
            <Col lg={ 3 } md={ 6 }>
                <CardSystem
                    title="Tasks Completed"
                    unit="Kb"
                    badgeColor="success"
                    pieColor="black"
                    badgeColor="#e375a7"
                    value="300"
                    icon="fa fa-tasks"
               />
            </Col>
            <Col lg={ 3 } md={ 6 }>
                <CardSystem
                    title="Active Tasks"
                    unit="Kb"
                    pieColor="black"
                    badgeColor="#f7bf47ad"
                    value="80"
                    icon="fa fa-tasks"
               />
            </Col>
            <Col lg={ 12 }>
                 <div style={{marginTop:"20px"}}>
                    <ChartsExample />
                 </div>
               
            </Col>
        </Row>
    </Container>
);

export default Dashboard;