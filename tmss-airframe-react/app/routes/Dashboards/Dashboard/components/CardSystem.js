import React from 'react';
import PropTypes from 'prop-types';

import {  
    Card, 
    CardBody,
    Badge
} from './../../../../components';

import {
    TinyDonutChart
} from "./TinyDonutChart"
import {
    TinyBarChart
} from "./TinyBarChart"

import { randomArray } from './../../../../utilities';

const percents = [
    "15",
    "25",
    "30",
    "35",
    "40",
    "45",
    "55",
    "60",
    "75",
    "80",
    "95"
];

const caret = [
    "down",
    "up"
];

const CardSystem = (props) => (
    <Card className="mb-3 mb-lg-0" style={{backgroundColor:  props.badgeColor,  }} >
       <CardBody className="pb-0 card-body"  > <i className={ props.icon} style={{ color:"black", fontSize:"50px"}}></i>
           <div className="d-flex">
               <span> 
               <h5 className="mb-2" style={{color: 'black'}}>
                        { props.title }
                    </h5>
                     
                   
                    <h2 className="mb-3">
                         { props.value }
                    </h2>
                </span>
                <span className="text-right ml-auto">
                    <TinyDonutChart 
                        pieColor={props.pieColor}
                    />
                </span>
            </div>
       </CardBody>
    </Card>
);

CardSystem.propTypes = {
    title: PropTypes.node,
    badgeColor: PropTypes.string,
    unit: PropTypes.node,
    pieColor: PropTypes.string,
    icon: PropTypes.string
};
CardSystem.defaultProps = {
    title: "Waiting...",
    badgeColor: "secondary",
    unit: "%",
    pieColor: "500"
};

export { CardSystem };
