import namor from 'namor'
import { Button} from 'react-bootstrap';
import jdata from './../../../../data/cycletbl.json';
import { randomArray } from './../../../../utilities';
const range = len => {
  const arr = []
  for (let i = 0; i < len; i++) {
    arr.push(i)
  }
  return arr
}

const rowarray = [
    0,1,2,3
];

 
const newCycle = () => {
   
    let tableData = jdata.data; 
    //console.log('tt  '+datearray[Math.floor(Math.random() * 11)]);
  const statusChance = Math.random()
  //console.log('----- '+tableData.length);
  let i = randomArray(rowarray)
  return {
    name: tableData[i].name,
   // start: namor.generate({ words: 1, numbers: 0 }),
    start: tableData[i].start,
    stop: tableData[i].stop,
    stdhrs: tableData[i].stdhrs,
    exphrs: tableData[i].exphrs,
    fillerhrs: tableData[i].filhrs,
    project: tableData[i].projects,
    editdata: tableData[i],
    status:
      statusChance > 0.66
        ? 'Completed'
        : statusChance > 0.33
        ? 'Hold'
        : 'In Progress',
   
  }
}


export default function makeData(...lens) {
  const makeDataLevel = (depth = 0) => {
    const len = lens[depth]
    return range(len).map(d => {
      return {
        ...newCycle(),
        subRows: lens[depth + 1] ? makeDataLevel(depth + 1) : undefined,
      }
    })
  }

  return makeDataLevel()
}
