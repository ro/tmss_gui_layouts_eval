import React from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import filterFactory, { Comparator, dateFilter } from 'react-bootstrap-table2-filter'
import ToolkitProvider from 'react-bootstrap-table2-toolkit';
import _ from 'lodash';
import faker from 'faker/locale/en_US';
import moment from 'moment';

import {
    Badge,
    Button,
    CustomInput,
    StarRating,
    ButtonGroup
} from './../../../../components';
import { CustomExportCSV } from './../../../Tables/ExtendedTable/components/CustomExportButton';
import { CustomSearch } from './../../../Tables/ExtendedTable/components/CustomSearch';
import { CustomPaginationPanel } from './../../../Tables/ExtendedTable/components/CustomPaginationPanel';
import { CustomSizePerPageButton } from './../../../Tables/ExtendedTable/components/CustomSizePerPageButton';
import { CustomPaginationTotal } from './../../../Tables/ExtendedTable/components/CustomPaginationTotal';
import { randomArray } from './../../../../utilities';
import {
    buildCustomTextFilter,
    buildCustomSelectFilter,
    buildCustomNumberFilter
} from './../../../Tables/ExtendedTable/filters';

import { 
    UncontrolledButtonDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Media,
    Avatar,
    AvatarAddOn
} from './../../../../components';

import JData from './../../../data/dashboardtbl.json';

const INITIAL_PRODUCTS_COUNT = 4;

const ProductQuality = {
    Good: 'product-quality__good',
    Bad: 'product-quality__bad',
    Unknown: 'product-quality__unknown'
};

const sortCaret = (order) => {
    if (!order)
        return <i className="fa fa-fw fa-sort text-muted"></i>;
    if (order)
        return <i className={`fa fa-fw text-muted fa-sort-${order}`}></i>
}

const generateRow = (index) => ({
    id: index,
    name: randomArray(['SCH-01', 'SCH-02', 'SCH-03', 'SCH-04']),
    inStockDate: faker.date.past(),
    quality:  randomArray(['16', '14', '12', '17']),
    price: randomArray(['5', '7', '9', '7']),
    satisfaction: randomArray(['50', '70', '0', '40'])
    
});

export class CycleTable extends React.Component {
    constructor() {
        super();
        
        this.state = {
            products: _.times(INITIAL_PRODUCTS_COUNT, generateRow),
            selected: [],
            jdataarry: [
                { "id": 1, "name": "SCH-01", "cycle": "LC1", "project": "Proj-01" , "unit":"2", "state":"Finished"},
                { "id": 2, "name": "SCH-02", "cycle": "LC2", "project": "Proj-02", "unit":"2", "state":"Approved"},
                { "id": 3, "name": "SCH-03", "cycle": "LC1", "project": "Proj-02", "unit":"2", "state":"In Progress"},
                { "id": 4, "name": "SCH-04", "cycle": "LC1", "project": "Proj-03" , "unit":"2", "state":"Defined"}
            ]
        };

        this.headerCheckboxRef = React.createRef();
    }

    handleSelect(row, isSelected) {
        if (isSelected) {
            this.setState({ selected: [...this.state.selected, row.id] })
        } else {
            this.setState({
                selected: this.state.selected.filter(itemId => itemId !== row.id)
            })
        }
    }

    handleSelectAll(isSelected, rows) {
        if (isSelected) {
            this.setState({ selected: _.map(rows, 'id') })
        } else {
            this.setState({ selected: [] });
        }
    }

    handleAddRow() {
        const currentSize = this.state.products.length;

        this.setState({
            products: [
                generateRow(currentSize + 1),
                ...this.state.products,
            ]
        });
    }

    handleDeleteRow() {
        this.setState({
            products: _.filter(this.state.products, product =>
                !_.includes(this.state.selected, product.id))
        })
    }

    handleResetFilters() {
        this.nameFilter('');
        this.qualityFilter('');
        this.priceFilter('');
        this.satisfactionFilter('');
    }

    createColumnDefinitions() {
        return [{
            dataField: 'name',
            text: 'Name',
            sort: true,
            sortCaret,
            formatter: (cell) => (
                <span className="text-inverse">
                    { cell }
                </span>
            ),
            ...buildCustomTextFilter({
                placeholder: 'Enter cycle name...',
                getFilter: filter => { this.nameFilter = filter; }
            })
        },{
            dataField: 'inStockDate',
            text: 'Start',
            formatter: (cell) =>
                moment(cell).format('DD MMM YYYY'),
            filter: dateFilter({
                className: 'd-flex align-items-center',
                comparatorClassName: 'd-none',
                dateClassName: 'form-control form-control-sm',
                comparator: Comparator.GT,
                getFilter: filter => { this.stockDateFilter = filter; }
            }),
            sort: true,
            sortCaret
         
        }, {
            dataField: 'inStockDate',
            text: 'Stop',
            formatter: (cell) =>
                moment(cell).format('DD MMM YYYY'),
            filter: dateFilter({
                className: 'd-flex align-items-center',
                comparatorClassName: 'd-none',
                dateClassName: 'form-control form-control-sm',
                comparator: Comparator.GT,
                getFilter: filter => { this.stockDateFilter = filter; }
            }),
            sort: true,
            sortCaret
        }, {
            dataField: 'price',
            text: 'Standard Hrs-',
            sort: true,
            sortCaret 
             
        }, {
            dataField: 'quality',
            text: 'Expert Hrs',
            sort: true,
            sortCaret 
            
        }, {
            dataField: 'id',
            text: 'Filler Hrs',
            sort: true,
            sortCaret 
        },
        {
            dataField: 'satisfaction',
            text: 'Projects',
            sort: true,
            sortCaret 
        } 
    ]; 
    }

    render() {
        
        const columnDefs = this.createColumnDefinitions();
        const paginationDef = paginationFactory({
            paginationSize: 5,
            showTotal: true,
            pageListRenderer: (props) => (
                <CustomPaginationPanel { ...props } size="sm" className="ml-md-auto mt-2 mt-md-0" />
            ),
            sizePerPageRenderer: (props) => (
                <CustomSizePerPageButton { ...props } />
            ),
            paginationTotalRenderer: (from, to, size) => (
                <CustomPaginationTotal { ...{ from, to, size } } />
            )
        });
        const selectRowConfig = {
            mode: 'checkbox',
            selected: this.state.selected,
            onSelect: this.handleSelect.bind(this),
            onSelectAll: this.handleSelectAll.bind(this),
            selectionRenderer: ({ mode, checked, disabled }) => (
                <CustomInput type={ mode } checked={ checked } disabled={ disabled } />
            ),
            selectionHeaderRenderer: ({ mode, checked, indeterminate }) => (
                <CustomInput type={ mode } checked={ checked } innerRef={el => el && (el.indeterminate = indeterminate)} />
            )
        };

        return (
            <ToolkitProvider
                keyField="id"
                data={ this.state.products }
                columns={ columnDefs }
                search
                exportCSV
            >
            {
                props => (
                    <React.Fragment>
                        <div className="d-flex justify-content-end align-items-center mb-2">
                             
                            <div className="d-flex ml-auto">
                                <CustomSearch
                                    className="mr-2"
                                    { ...props.searchProps }
                                />
                                <ButtonGroup>
                                    <CustomExportCSV
                                        { ...props.csvProps }
                                    >
                                        Export
                                    </CustomExportCSV>
                                    <Button
                                        size="sm"
                                        outline
                                        onClick={ this.handleDeleteRow.bind(this) }
                                    >
                                        Delete
                                    </Button>
                                    <Button
                                        size="sm"
                                        outline
                                        onClick={ this.handleAddRow.bind(this) }
                                    >
                                        <i className="fa fa-fw fa-plus"></i>
                                    </Button>
                                </ButtonGroup>
                            </div>
                        </div>
                        <BootstrapTable
                            classes="table-responsive"
                            pagination={ paginationDef }
                            filter={ filterFactory() }
                            selectRow={ selectRowConfig }
                            bordered={ false }
                            responsive
                            { ...props.baseProps }
                        />
                    </React.Fragment>
                )
            }
            </ToolkitProvider>
             
        );
    }
}