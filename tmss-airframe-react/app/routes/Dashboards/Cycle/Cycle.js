import React from 'react'
import styled from 'styled-components'
import { 
    UncontrolledButtonDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Form, 
    Col,
    FormGroup, 
    Label, 
    Input, 
    FormText, 
    UncontrolledModal,
    ModalHeader,
    CardBody,
    CardTitle,
    CardGroup,
    ModalBody,
    ModalFooter
} from './../../../../app/components';

import { 
    useTable,
    usePagination,
    useSortBy,
    useFilters,
    useGroupBy,
    useExpanded,
    useRowSelect,
    useGlobalFilter, 
    useAsyncDebounce,  } from 'react-table'
// A great library for fuzzy filtering/sorting items
import matchSorter from 'match-sorter'
import {Button, Collapse} from 'react-bootstrap'
import makeData from './components/MakeData'
/*eslint-enable */
import { HeaderMain } from "../../components/HeaderMain";

import {checkRolePermisson} from './../../../utilities';

const Styles = styled.div`
  padding: 1rem;

  table {
    border-spacing: 0;
    border: 1px solid black;
    box-sizing: border-box;
    width: 100%;
    border-color: lightgray;
    background-color: white;

    tr {
      :last-child {
        td {
          border-right: 0;
        }
      }
    }

    th{
        color: black;
        margin: 0;
        padding: 0.5rem;
        border-bottom: 1px solid lightgray;
		
		
    }
    td {
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px solid lightgray;
	  
      :last-child {
          border-right: 0;
      }
    }
  }
`


// Define a default UI for filtering
function GlobalFilter({
  preGlobalFilteredRows,
  globalFilter,
  setGlobalFilter,
}) {
  const count = preGlobalFilteredRows.length
  const [value, setValue] = React.useState(globalFilter)
  const onChange = useAsyncDebounce(value => {
    setGlobalFilter(value || undefined)
  }, 200)

  return (
    <span>
      Table Content Search:{' '}
      <input 
        value={value || ""}
        onChange={e => {
          setValue(e.target.value);
          onChange(e.target.value);
        }}
        placeholder={`${count} records...`}
        style={{
            backgroundColor: 'honeydew',
          fontSize: '1.1rem',
          border: '1',
        }}
      />
    </span>
  )
}

// Define a default UI for filtering
function DefaultColumnFilter({
  column: { filterValue, preFilteredRows, setFilter },
}) {
  const count = preFilteredRows.length

  return (
    <input
      value={filterValue || ''}
      onChange={e => {
        setFilter(e.target.value || undefined) // Set undefined to remove the filter entirely
      }}
    //   placeholder={`Search ${count} records...`} 
    />
  )
}

// This is a custom filter UI for selecting
// a unique option from a list
function SelectColumnFilter({
  column: { filterValue, setFilter, preFilteredRows, id },
}) {
  // Calculate the options for filtering
  // using the preFilteredRows
  const options = React.useMemo(() => {
    const options = new Set()
    preFilteredRows.forEach(row => {
      options.add(row.values[id])
    })
    return [...options.values()]
  }, [id, preFilteredRows])

  // Render a multi-select box
  return (
    <select
      value={filterValue}
      onChange={e => {
        setFilter(e.target.value || undefined)
      }}
    >
      <option value="">All</option>
      {options.map((option, i) => (
        <option key={i} value={option}>
          {option}
        </option>
      ))}
    </select>
  )
}

// This is a custom filter UI that uses a
// slider to set the filter value between a column's
// min and max values
function SliderColumnFilter({
  column: { filterValue, setFilter, preFilteredRows, id },
}) {
  // Calculate the min and max
  // using the preFilteredRows

  const [min, max] = React.useMemo(() => {
    let min = preFilteredRows.length ? preFilteredRows[0].values[id] : 0
    let max = preFilteredRows.length ? preFilteredRows[0].values[id] : 0
    preFilteredRows.forEach(row => {
      min = Math.min(row.values[id], min)
      max = Math.max(row.values[id], max)
    })
    return [min, max]
  }, [id, preFilteredRows])

  return (
    <>
      <input
        type="range"
        min={min}
        max={max}
        value={filterValue || min}
        onChange={e => {
          setFilter(parseInt(e.target.value, 10))
        }}
      />
      <button onClick={() => setFilter(undefined)}>Off</button>
    </>
  )
}

// This is a custom UI for our 'between' or number range
// filter. It uses two number boxes and filters rows to
// ones that have values between the two
function NumberRangeColumnFilter({
  column: { filterValue = [], preFilteredRows, setFilter, id },
}) {
  const [min, max] = React.useMemo(() => {
    let min = preFilteredRows.length ? preFilteredRows[0].values[id] : 0
    let max = preFilteredRows.length ? preFilteredRows[0].values[id] : 0
    preFilteredRows.forEach(row => {
      min = Math.min(row.values[id], min)
      max = Math.max(row.values[id], max)
    })
    return [min, max]
  }, [id, preFilteredRows])

  return (
    <div
      style={{
        display: 'flex',
      }}
    >
      <input
        value={filterValue[0] || ''}
        type="number"
        onChange={e => {
          const val = e.target.value
          setFilter((old = []) => [val ? parseInt(val, 10) : undefined, old[1]])
        }}
        placeholder={`Min (${min})`}
        style={{
          width: '70px',
          marginRight: '0.5rem',
        }}
      />
      to
      <input
        value={filterValue[1] || ''}
        type="number"
        onChange={e => {
          const val = e.target.value
          setFilter((old = []) => [old[0], val ? parseInt(val, 10) : undefined])
        }}
        placeholder={`Max (${max})`}
        style={{
          width: '70px',
          marginLeft: '0.5rem',
        }}
      />
    </div>
  )
}

function fuzzyTextFilterFn(rows, id, filterValue) {
  return matchSorter(rows, filterValue, { keys: [row => row.values[id]] })
}

// Let the table remove the filter if the string is empty
fuzzyTextFilterFn.autoRemove = val => !val

const IndeterminateCheckbox = React.forwardRef(
    ({ indeterminate, ...rest }, ref) => {
      const defaultRef = React.useRef()
      const resolvedRef = ref || defaultRef
  
      React.useEffect(() => {
        resolvedRef.current.indeterminate = indeterminate
      }, [resolvedRef, indeterminate])
  
      return <input type="checkbox" ref={resolvedRef} {...rest} />
    }
  )

// Our table component
function Table({ columns, data }) {
  const filterTypes = React.useMemo(
    () => ({
      // Add a new fuzzyTextFilterFn filter type.
      fuzzyText: fuzzyTextFilterFn,
      // Or, override the default text filter to use
      // "startWith"
      text: (rows, id, filterValue) => {
        return rows.filter(row => {
          const rowValue = row.values[id]
          return rowValue !== undefined
            ? String(rowValue)
                .toLowerCase()
                .startsWith(String(filterValue).toLowerCase())
            : true
        })
      },
    }),
    []
  )

  const defaultColumn = React.useMemo(
    () => ({
      // Let's set up our default Filter UI
      Filter: DefaultColumnFilter,
    }),
    []
  )

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
	  allColumns,
    getToggleHideAllColumnsProps,
    state,
    visibleColumns,
    preGlobalFilteredRows,
    setGlobalFilter,
    page,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    state: { pageIndex, pageSize }
  } = useTable(
    {
      columns,
      data,
      defaultColumn, // Be sure to pass the defaultColumn option
      filterTypes,
      initialState: { pageIndex: 0 }
    },
    useFilters, // useFilters!
    useGlobalFilter, // useGlobalFilter!
    useSortBy,
    usePagination
  )

  // We don't want to render all of the rows for this example, so cap
  // it for this use case
  const firstPageRows = rows.slice(0, 10)
    
  
  function changestate(){
      var divid = document.getElementById('tagleid');
      if (divid.style.display === "none") {
        divid.style.display = "block";
      } else {
        divid.style.display = "none";
      }
  }
  return (
    <>
    <HeaderMain 
                title="Cycles"
            />
           <UncontrolledButtonDropdown style={{float: 'right'}}>
              <DropdownToggle color="link" className={` text-decoration-none 'green' `}>
              <i class="fa fa-bars" aria-hidden="true"></i>
              </DropdownToggle>
              <DropdownMenu right>
                <div style={{float: 'left', backgroundColor: '#d1cdd936', width: '250px'}}>
                  <div id="tagleid"  >
                    <h5><p>Select column(s) to view</p></h5>
                    <div >
                    <div style={{marginBottom:'5px'}}>
                    <IndeterminateCheckbox {...getToggleHideAllColumnsProps()} /> Select All
                    </div>
                    {allColumns.map(column => (
                    <div key={column.id}>
                      <label style={{color:'black'}}>
                      <input type="checkbox" {...column.getToggleHiddenProps()} />{' '}
                      {column.id}
                      </label>
                    </div>
                    ))}
                    <br />
                  </div>
                  </div>
                </div>
              </DropdownMenu>
            </UncontrolledButtonDropdown>
      <table {...getTableProps()}>
        <thead>
       {/* <tr  > 
            <th
              colSpan={visibleColumns.length}
              style={{
                textAlign: 'right',
              }}
            >
              <GlobalFilter
                preGlobalFilteredRows={preGlobalFilteredRows}
                globalFilter={state.globalFilter}
                setGlobalFilter={setGlobalFilter}
              />
            </th>
            </tr> */}
          {headerGroups.map(headerGroup => (
            <tr {...headerGroup.getHeaderGroupProps()}  >
              {headerGroup.headers.map(column => (
                <th {...column.getHeaderProps(column.getSortByToggleProps())}>
                  {column.render('Header')}
                  <span>
                  {column.isSorted ? (column.isSortedDesc ? <i class="fa fa-sort-desc" aria-hidden="true"></i> : <i class="fa fa-sort-asc" aria-hidden="true"></i>) : ""}
                </span>
                  {/* Render the columns filter UI */}
                  <div>{column.canFilter ? column.render('Filter') : null}</div>
                </th>
              ))}
            </tr>
          ))}
          
        </thead>
        <tbody {...getTableBodyProps()}>
          {firstPageRows.map((row, i) => {
            prepareRow(row)
            return (
              <tr {...row.getRowProps()}>
                {row.cells.map(cell => {
                  return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                })}
              </tr>
            )
          })}
        </tbody>
      </table>
       <br/>
      <div>
        <button onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
          {"<<"}
        </button>{" "}
        <button onClick={() => previousPage()} disabled={!canPreviousPage}>
          {"<"}
        </button>{" "}
        <button onClick={() => nextPage()} disabled={!canNextPage}>
          {">"}
        </button>{" "}
        <button onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}>
          {">>"}
        </button>{" "}
        <span>
          Page{" "}
          <strong>
            {pageIndex + 1} of {pageOptions.length}
          </strong>{" "}
        </span>
        <span>
          | Go to page:{" "}
          <input
            type="number"
            defaultValue={pageIndex + 1}
            onChange={e => {
              const page = e.target.value ? Number(e.target.value) - 1 : 0;
              gotoPage(page);
            }}
            style={{ width: "100px" }}
          />
        </span>{" "}
        <select
          value={pageSize}
          onChange={e => {
            setPageSize(Number(e.target.value));
          }}
        >
          {[10, 20, 30, 40, 50].map(pageSize => (
            <option key={pageSize} value={pageSize}>
              Show {pageSize}
            </option>
          ))}
        </select>
      </div>
      
      <br />
     {/* <div>Showing the first 20 results of {rows.length} rows</div>
      <div>
        <pre>
          <code>{JSON.stringify(state.filters, null, 2)}</code>
        </pre>
      </div> */}
    </>
  )
}

// Define a custom filter filter function!
function filterGreaterThan(rows, id, filterValue) {
  return rows.filter(row => {
    const rowValue = row.values[id]
    return rowValue >= filterValue
  })
}

// This is an autoRemove method on the filter function that
// when given the new filter value and returns true, the filter
// will be automatically removed. Normally this is just an undefined
// check, but here, we want to remove the filter if it's not a number
filterGreaterThan.autoRemove = val => typeof val !== 'number'

function Cycle() {
  const columns = React.useMemo(
    () => [
      {
        Header: ' ',
        columns: [
            {
                Header: 'Name',
                accessor: 'name',
                Filter: SelectColumnFilter,
            filter: 'includes',
                minWidth: '75px',
              },
            {
                Header: 'Start',
                accessor: 'start',
                disableFilters: false,
               
                },
                {
                Header: 'Stop',
                accessor: 'stop',
                disableFilters: false,
                disableSortBy: false,
                },
              {
                Header: 'Standard Hrs',
                accessor: 'stdhrs',
                Filter: SliderColumnFilter,
                filter: filterGreaterThan,
                 
              },
              {
                Header: 'Expert Hrs',
                accessor: 'exphrs',
               // disableSortBy: true,
                Cell: row => <div style={{ textAlign: "center" }}>{row.value}</div>,
                minWidth: "75px",
                 
              },
              {
                Header: 'Filler Hrs',
                accessor: 'fillerhrs',
                Cell: row => <div style={{ textAlign: "center" }}>{row.value}</div>,
                minWidth: "75px",
              },
              {
                Header: 'Project ID',
                accessor: 'project',
                Cell: row => <div style={{ textAlign: "center" }}>{row.value}</div>,
                minWidth: "75px",
              },
          {
            Header: 'Action',
            accessor: 'editdata',
            disableFilters: true,
            disableSortBy: true,
            Cell: row => <UncontrolledButtonDropdown>
            <DropdownToggle color="link" className={` text-decoration-none 'green' `}>
                <i className="fa fa-gear"></i><i className="fa fa-angle-down ml-2"></i>
            </DropdownToggle>
            <DropdownMenu right >
              {checkRolePermisson('Cycles', 'add') &&
                <DropdownItem>
                    <i className="fa fa-plus mr-2"></i>
                    Add Project
                </DropdownItem>
                }
                 {checkRolePermisson('Cycles', 'edit') &&
                <DropdownItem   id="editcycle" >
                    <i className="fa fa-pencil fa-fw mr-2"></i>
                    Edit Cycle
                </DropdownItem>
                  
                }
                <UncontrolledModal target="editcycle" >
                  <ModalBody >
                    <div style={{float: 'left'}}>
                      <div >
                      <h3> Add Project</h3> 
                      </div> 
                      <Form>		 
                        <FormGroup row>
                          <Label for="input-1" sm={4}>
                            Name
                          </Label>
                          <Col sm={8}>
                          <Input 
                            type="text" 
                            name="" 
                            id="input-1" 
                            placeholder="Enter Project Name..." 
                            />
                          </Col>
                        </FormGroup>			 
                        <FormGroup row>
                          <Label for="inputPassword-1" sm={4}>
                            Priority
                          </Label>
                          <Col sm={8}>
                            <Input 
                            type="number" 
                            name="" 
                            id="inputPassword-1" 
                            placeholder="Priority" 
                          />
                          </Col>
                        </FormGroup>
                      </Form>
                    </div>
                  </ModalBody>
                  <ModalFooter>
                    <UncontrolledModal.Close   color="secondary">
                      Close
                    </UncontrolledModal.Close>
                    <Button color="info">Save</Button>
                  </ModalFooter>
                </UncontrolledModal>



            </DropdownMenu>
        </UncontrolledButtonDropdown>,
          },
        ],
      },
    ],
    []
  )
  
  const data = React.useMemo(() => makeData(1000), [])

  return (
    <Styles>
      <Table columns={columns} data={data} striped bordered hover />
    </Styles>
  )
}

export default Cycle
