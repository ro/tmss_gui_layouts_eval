export const schemaDB ={
    taskSchema:  {
    "$id": "http://example.com/example.json",
    "type": "object",
    "$schema": "http://json-schema.org/draft-07/schema#",
    "definitions": {
      "pointing": {
        "type": "object",
        "additionalProperties": false,
        "format": "grid",
        "properties": {
          "direction_type": {
            "type": "string",
            "title": "Reference frame",
            "description": "",
            "default": "J2000",
            "enum": [
              "J2000",
              "SUN",
              "MOON",
              "MERCURY",
              "VENUS",
              "MARS",
              "JUPITER",
              "SATURN",
              "URANUS",
              "NEPTUNE",
              "PLUTO"
            ]
          },
          "angle1": {
            "type": "number",
            "title": "Angle 1",
            "description": "First angle (f.e. RA)",
            "default": 0
          },
          "angle2": {
            "type": "number",
            "title": "Angle 2",
            "description": "Second angle (f.e. DEC)",
            "default": 0
          }
        },
        "required": [
          "angle1",
          "angle2"
        ]
      }
    },
    "format": "grid",
    "additionalProperties": false,
    "properties": {
      "stations": {
        "title": "Station list",
        "oneOf": [
          {
            "type": "array",
            "title": "Fixed list",
            "format": "grid",
            "additionalItems": false,
            "additionalProperties": false,
            "items": {
              "type": "string",
              "enum": [
                "CS001",
                "CS002",
                "CS003",
                "CS004",
                "CS005",
                "CS006",
                "CS007",
                "CS011",
                "CS013",
                "CS017",
                "CS021",
                "CS024",
                "CS026",
                "CS028",
                "CS030",
                "CS031",
                "CS032",
                "CS101",
                "CS103",
                "CS201",
                "CS301",
                "CS302",
                "CS401",
                "CS501",
                "RS104",
                "RS106",
                "RS205",
                "RS208",
                "RS210",
                "RS305",
                "RS306",
                "RS307",
                "RS310",
                "RS406",
                "RS407",
                "RS409",
                "RS410",
                "RS503",
                "RS508",
                "RS509",
                "DE601",
                "DE602",
                "DE603",
                "DE604",
                "DE605",
                "FR606",
                "SE607",
                "UK608",
                "DE609",
                "PL610",
                "PL611",
                "PL612",
                "IE613",
                "LV614"
              ],
              "title": "Station",
              "description": ""
            },
            "minItems": 1,
            "uniqueItems": true
          },
          {
            "title": "Dynamic list",
            "type": "array",
            "format": "tabs",
            "additionalItems": false,
            "items": {
              "type": "object",
              "format": "grid",
              "title": "Station set",
              "headerTemplate": "{{ self.group }}",
              "additionalProperties": false,
              "properties": {
                "group": {
                  "type": "string",
                  "title": "Group/station",
                  "description": "Which (group of) station(s) to select from",
                  "default": "ALL",
                  "enum": [
                    "ALL",
                    "SUPERTERP",
                    "CORE",
                    "REMOTE",
                    "DUTCH",
                    "INTERNATIONAL"
                  ]
                },
                "min_stations": {
                  "type": "integer",
                  "title": "Minimum nr of stations",
                  "description": "Number of stations to use within group/station",
                  "default": 1,
                  "minimum": 0
                }
              },
              "required": [
                "group",
                "min_stations"
              ]
            }
          }
        ]
      },
      "antenna_set": {
        "type": "string",
        "title": "Antenna set",
        "description": "Fields & antennas to use",
        "default": "HBA_DUAL",
        "enum": [
          "HBA_DUAL",
          "HBA_DUAL_INNER",
          "HBA_ONE",
          "HBA_ONE_INNER",
          "HBA_ZERO",
          "HBA_ZERO_INNER",
          "LBA_INNER",
          "LBA_OUTER",
          "LBA_SPARSE_EVEN",
          "LBA_SPARSE_ODD",
          "LBA_ALL"
        ]
      },
      "filter": {
        "type": "string",
        "title": "Band-pass filter",
        "description": "Must match antenna type",
        "default": "HBA_110_190",
        "enum": [
          "LBA_10_70",
          "LBA_30_70",
          "LBA_10_90",
          "LBA_30_90",
          "HBA_110_190",
          "HBA_210_250"
        ]
      },
      "analog_pointing": {
        "title": "Analog pointing",
        "description": "HBA only",
        "$ref": "#/definitions/pointing"
      },
      "beams": {
        "type": "array",
        "title": "Beams",
        "format": "tabs",
        "additionalItems": false,
        "items": {
          "title": "Beam",
          "headerTemplate": "{{ i0 }} - {{ self.name }}",
          "type": "object",
          "additionalProperties": false,
          "properties": {
            "name": {
              "type": "string",
              "title": "Name/target",
              "description": "Identifier for this beam",
              "default": ""
            },
            "digital_pointing": {
              "title": "Digital pointing",
              "$ref": "#/definitions/pointing"
            },
            "subbands": {
              "type": "array",
              "title": "Subband list",
              "format": "table",
              "additionalItems": false,
              "items": {
                "type": "integer",
                "title": "Subband",
                "minimum": 0,
                "maximum": 511
              }
            }
          },
          "required": [
            "digital_pointing",
            "subbands"
          ]
        }
      },
      "duration": {
        "type": "number",
        "title": "Duration (seconds)",
        "description": "Duration of this observation",
        "default": 300,
        "minimum": 1
      },
      "correlator": {
        "title": "Correlator Settings",
        "type": "object",
        "additionalProperties": false,
        "properties": {
          "channels_per_subband": {
            "type": "integer",
            "title": "Channels/subband",
            "description": "Number of frequency bands per subband",
            "default": 64,
            "minimum": 8,
            "enum": [
              8,
              16,
              32,
              64,
              128,
              256,
              512,
              1024
            ]
          },
          "integration_time": {
            "type": "number",
            "title": "Integration time (seconds)",
            "description": "Desired integration period",
            "default": 1,
            "minimum": 0.1
          },
          "storage_cluster": {
            "type": "string",
            "title": "Storage cluster",
            "description": "Cluster to write output to",
            "default": "CEP4",
            "enum": [
              "CEP4",
              "DragNet"
            ]
          }
        },
        "required": [
          "channels_per_subband",
          "integration_time",
          "storage_cluster"
        ]
      }
    },
    "required": [
      "stations",
      "antenna_set",
      "filter",
      "beams",
      "duration",
      "correlator"
    ]
  }
}

export default schemaDB;