/**
 * This is the JSON Editor component created by integrating
 * "@json-editor/json-editor" for editing JSON based on JSON Schema and
 * 'react-json-view' for viewing its output JSON 
 */
import React, {Component} from 'react';

// Header Component of Airframe. Should be changed as per the framework or library used
import {HeaderMain} from '../components/HeaderMain';

// Custom components with editor and viewer
import Jeditor from './JEditor';
import Jviewer from './JViewer';

export class JSONEditor extends Component {
    constructor(props) {
        super(props);
        this.state = {
            outputJSON: {},
        };
        this.setOutput = this.setOutput.bind(this);
    }

    /**
     * Call back function for JEditor to pass the form data
     * back to the parent component and update the viewer component
     * @param {*} output 
     */
    setOutput(output) {
        this.setState({outputJSON: output});
        this.viewer.updateOutput(output);
    }
    
    render() {
        // Create JEditor component with callback as the props
        const jeditor = React.createElement(Jeditor, {callback: this.setOutput});
        return(
            <React.Fragment>
                <HeaderMain 
                    title="JSON Editor"
                />
                {jeditor}
                <div style={{marginTop: "10px", padding: "10px", border:"1px solid #DEE2E6"}}>
                    <Jviewer outputCallback={this.state.outputJSON} ref={(viewer) => {this.viewer=viewer}}></Jviewer>
                </div>
            </React.Fragment>
        );
    }
}