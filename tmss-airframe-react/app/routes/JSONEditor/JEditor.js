/**
 * This is the custom component to use "@json-editor/json-editor"
 * to create form using JSON Schema and get JSON output
 */
import React, {useEffect, useState} from 'react';

// Sample JSON schema. The same can be received from an API call
import SchemaDB from './task.observation.schema';

// import "@fortawesome/fontawesome-free/css/all.css";
import '../../styles/components/fa_all.scss';
const JSONEditor = require("@json-editor/json-editor").JSONEditor;

function Jeditor(props) {
    // State object to hold the editor reference to get output
    const [stateEditor, setStateEditor] = useState({editor:null});
    let editor = null;
    useEffect(() => {
        const element = document.getElementById('editor_holder');
        const schema = SchemaDB.taskSchema;
        editor = new JSONEditor(element, {
            schema: schema,
            display_required_only: true, // circular references will blow up without this
            theme: 'bootstrap4',
            iconlib: 'fontawesome5'
        });
        setStateEditor({editor: editor});
    }, []);

    /**
     * Function to call on button click and send the output back to parent through callback
     * 
     */
    function setEditorOutput(){
        const output = stateEditor.editor.getValue();
        props.callback(output);
    }

    return (
        <React.Fragment>
            <div id='editor_holder'></div>
            <div><input type="button" onClick={setEditorOutput} value="Show Output" /></div>
        </React.Fragment>
    );
};

export default Jeditor;