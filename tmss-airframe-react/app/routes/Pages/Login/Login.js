import React from 'react';
import { Link, Redirect  } from 'react-router-dom';

import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { authenticationService } from '../../../_services/authentication.service';
import {
   
    FormGroup,
    FormText,
    Input,
    CustomInput,
    Button,
    Label,
    EmptyLayout,
    ThemeConsumer
} from './../../../components';

import { HeaderAuth } from "../../components/Pages/HeaderAuth";
import { FooterAuth } from "../../components/Pages/FooterAuth";
import Cookies from 'universal-cookie';
 
//const Login = () => (
 class Login extends React.Component {

    rdir = "/tables/cycletables";
     constructor(props){
         super(props);
         this.state= {
             rname: '/tables/cycletables',
             username: '',
             password: ''
         }

         // redirect to home if already logged in
        if (authenticationService.currentUserValue) { 
            this.props.history.push('/dashboards/dashboard');
            console.log(authenticationService.currentUserValue.results);
        }
        
         this.loginprocess = this.loginprocess.bind(this);
     }
  
     loginprocess=()=>{
        const cookies = new Cookies();
        
      let un = document.getElementById('emailAdress').value;
      let pa = document.getElementById('password').value;
      if(un == 'admin' && pa == 'admin'){
        location.href ="/dashboards/dashboard" ;
        cookies.set('bcpath', '/dashboards/dashboard' );  
        cookies.set('bcname','Dashboard');  
        cookies.set('username', 'admin' );
        cookies.set('job-role', 'Astron TMSS Admin' );
      }else if(un == 'astronomer' && pa == 'astronomer'){
        location.href ="/tables/cycletables" ;
        cookies.set('bcpath', '/tables/cycletables' );
        cookies.set('bcname','Cycle');  
        cookies.set('username', 'astronomer' );
        cookies.set('job-role', 'TMSS Astronomer' );
      }else if(un == 'sos' && pa == 'sos'){
        location.href ="/tables/extended-table" ;
        cookies.set('bcname','Project');  
        cookies.set('bcpath', '/tables/extended-table' );
        cookies.set('username', 'sos' );
        cookies.set('job-role', 'TMSS SOS' );
      }else if(un == 'operator' && pa == 'operator'){
        location.href ="/dashboards/projects" ;
        cookies.set('bcpath', '/dashboards/projects' );
        cookies.set('bcname','Project'); 
        cookies.set('username', 'operator' );
        cookies.set('job-role', 'TMSS Operator' );
      }else{
        location.href ="/" ;
        cookies.set('bcpath',''); 
        cookies.set('bcname',''); 
        cookies.set('username', '' );
        cookies.set('job-role', '' );
      }

      console.log(cookies.get('username'));
     // alert(un+ '  '+pa);
      //  this.rdir = "/dashboards/dashboard";
     //   this.setState({rname: '/dashboards/dashboard'})
        // this.state.rname = "/dashboards/dashboard";
      //  alert(this.rdir); 
       
    }

        render(){
            return (
    <EmptyLayout >
        <EmptyLayout.Section center>
            { /* START Header */}
            <HeaderAuth 
                title=""
            />
            { /* END Header */}
            { /* START Form */}
            <div>
                {/* <img src={require({'astronlogo.jpg'}) alt="logo"/> */}
               <center><h2>Login Form</h2></center>
                <Formik
                    initialValues={{
                        username: '',
                        password: ''
                    }}
                    validationSchema={Yup.object().shape({
                        username: Yup.string().required('Username is required'),
                        password: Yup.string().required('Password is required')
                    })}
                    onSubmit={({ username, password }, { setStatus, setSubmitting }) => {
                        setStatus();
                        authenticationService.login(username, password)
                            .then(
                                user => {
                                    const { from } = this.props.location.state || { from: { pathname: "/dashboards/dashboard" } };
                                    this.props.history.push(from);
                                
                                
                                },
                                error => {
                                    setSubmitting(false);
                                    setStatus(error);
                                }
                            );
                    }}
                    render={({ errors, status, touched, isSubmitting }) => (
                        
                        <Form style={{width: '140%'}}>
                             {status &&
                                <div className={'alert alert-danger'}>{status}</div>
                            }
                            <div className="form-group">
                                <label htmlFor="username">Username</label>
                                <Field name="username" type="text" className={'form-control' + (errors.username && touched.username ? ' is-invalid' : '')} />
                                <ErrorMessage name="username" component="div" className="invalid-feedback" />
                            </div>
                            <div className="form-group">
                                <label htmlFor="password">Password</label>
                                <Field name="password" type="password" className={'form-control' + (errors.password && touched.password ? ' is-invalid' : '')} />
                                <ErrorMessage name="password" component="div" className="invalid-feedback" />
                            </div>
                            <div className="form-group">
                                <button type="submit" className="btn btn-primary" disabled={isSubmitting}>Login</button>
                                {isSubmitting &&
                                    <img alt ="" src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                                }
                            </div>
                           
                        </Form>
                    )}
                />
            </div>
            { /* END Form */}
            { /* START Bottom Links */}
            <div className="d-flex mb-5">
                <Link to="/pages/forgotpassword" className="text-decoration-none">
                    Forgot Password
                </Link>
                <Link to="/pages/register" className="ml-auto text-decoration-none">
                    Register
                </Link>
            </div>
            { /* END Bottom Links */}
            { /* START Footer */}
            <FooterAuth />
            { /* END Footer */}
        </EmptyLayout.Section>
         
    </EmptyLayout>
    )
}
}

 export default Login;
