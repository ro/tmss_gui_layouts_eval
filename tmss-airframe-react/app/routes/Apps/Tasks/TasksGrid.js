import React from 'react';

import { CardColumns } from './../../../components';
import { Paginations } from "../../components/Paginations";
import { TasksCardGrid } from "../../components/Tasks/TasksCardGrid";

const TasksGrid = () => (
    <React.Fragment>
        <CardColumns>
            <TasksCardGrid bgcolor="aquamarine" />
            <TasksCardGrid  
                id="2"bgcolor="#bdf390"
            />
            <TasksCardGrid bgcolor="#bdf390"
                id="3"
            />
            <TasksCardGrid bgcolor="#f3d790"
                id="4"
            />
            <TasksCardGrid bgcolor="#dfdfdf"
                id="5"
            />
            <TasksCardGrid bgcolor="aquamarine"
                id="6"
            />
            <TasksCardGrid bgcolor="#bdf390"
                id="7"
            />
            <TasksCardGrid bgcolor="powderblue"
                id="8"
            />
            <TasksCardGrid bgcolor="lightskyblue"
                id="9"
            />
        </CardColumns>
        <div className="d-flex justify-content-center">
            <Paginations />
        </div>
    </React.Fragment>
);

export default TasksGrid;