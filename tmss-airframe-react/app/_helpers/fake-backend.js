import { Role } from './role'

export function configureFakeBackend() {
    let users = [
        { id: 1, username: 'admin', password: 'admin', firstName: 'Admin', lastName: 'User', role: Role.Admin },
        { id: 2, username: 'user', password: 'user', firstName: 'Normal', lastName: 'User', role: Role.User },
        { id: 3, username: 'astronomer', password: 'astro', firstName: 'Astronomer', lastName: 'User', role: Role.Astronomer},
        { id: 4, username: 'operator', password: 'operator', firstName: 'Operator', lastName: 'User', role: Role.Operator},
        { id: 5, username: 'sos', password: 'sos', firstName: 'Sos', lastName: 'User', role: Role.Sos},
        { id: 6, username: 'dev', password: 'dev', firstName: 'UI', lastName: 'Developer', role: Role.Dev},
   ];

   let userRolePermissions = [ 
    {"id": 1, "module": "Cycles", "role": Role.Astronomer, "add": true, "edit": false, "view": false, "delete": false, "sidemenu":true},
{"id": 2, "module": "Projects", "role": Role.Astronomer, "add": false, "edit": true, "view": true, "delete": false, "sidemenu":true},
{"id": 3, "module": "Schedule", "role": Role.Astronomer, "add": true, "edit": true, "view": true, "delete": true, "sidemenu":true},
{"id": 4, "module": "Task", "role": Role.Astronomer, "add": true, "edit": true, "view": true, "delete": true, "sidemenu":false},
{"id": 5, "module": "Report", "role": Role.Astronomer, "add": true, "edit": true, "view": true, "delete": true, "sidemenu":true},
{"id": 6, "module": "Calendar", "role": Role.Astronomer, "add": true, "edit": false, "view": false, "delete": false, "sidemenu":true},
{"id": 7, "module": "Cycles", "role": Role.Sos, "add": true, "edit": true, "view": true, "delete": true, "sidemenu":false},
{"id": 8, "module": "Projects", "role": Role.Sos, "add": false, "edit": true, "view": true, "delete": true, "sidemenu":false},
{"id": 9, "module": "Schedule", "role": Role.Sos, "add": true, "edit": true, "view": true, "delete": true, "sidemenu":true},
{"id": 10, "module": "Task", "role": Role.Sos, "add": true, "edit": true, "view": true, "delete": true, "sidemenu":true},
{"id": 11, "module": "Report", "role": Role.Sos, "add": true, "edit": true, "view": true, "delete": true, "sidemenu":true},
{"id": 12, "module": "Calendar", "role": Role.Sos, "add": true, "edit": true, "view": true, "delete": true, "sidemenu":true},
{"id": 13, "module": "Cycles", "role": Role.Operator, "add": true, "edit": true, "view": true, "delete": true, "sidemenu":false},
{"id": 14, "module": "Projects", "role": Role.Operator, "add": true, "edit": true, "view": true, "delete": true, "sidemenu":false},
{"id": 15, "module": "Schedule", "role": Role.Operator, "add": true, "edit": true, "view": true, "delete": true, "sidemenu":false},
{"id": 16, "module": "Task", "role": Role.Operator, "add": true, "edit": true, "view": true, "delete": true, "sidemenu":true},
{"id": 17, "module": "Report", "role": Role.Operator, "add": true, "edit": true, "view": true, "delete": true, "sidemenu":false},
{"id": 18, "module": "Calendar", "role": Role.Operator, "add": true, "edit": true, "view": true, "delete": true, "sidemenu":true}, 
{"id": 19, "module": "Cycles", "role": Role.Admin, "add": true, "edit": true, "view": true, "delete": true, "sidemenu":true},
{"id": 20, "module": "Projects", "role": Role.Admin, "add": true, "edit": true, "view": true, "delete": true, "sidemenu":true},
{"id": 21, "module": "Schedule", "role": Role.Admin, "add": true, "edit": true, "view": true, "delete": true, "sidemenu":true},
{"id": 22, "module": "Task", "role": Role.Admin, "add": true, "edit": true, "view": true, "delete": true, "sidemenu":true},
{"id": 23, "module": "Report", "role": Role.Admin, "add": true, "edit": true, "view": true, "delete": true, "sidemenu":true},
{"id": 24, "module": "Calendar", "role": Role.Admin, "add": true, "edit": true, "view": true, "delete": true, "sidemenu":true},
{"id": 25, "module": "Cycles", "role": Role.Dev, "add": true, "edit": true, "view": true, "delete": true, "sidemenu":true}, 
{"id": 26, "module": "Projects", "role": Role.Dev, "add": true, "edit": true, "view": true, "delete": true, "sidemenu":true},
{"id": 27, "module": "Schedule", "role": Role.Dev, "add": true, "edit": true, "view": true, "delete": true, "sidemenu":true},  
{"id": 28, "module": "Task", "role": Role.Dev, "add": true, "edit": true, "view": true, "delete": true, "sidemenu":true},
{"id": 29, "module": "Report", "role": Role.Dev, "add": true, "edit": true, "view": true, "delete": true, "sidemenu":true},
{"id": 30, "module": "Calendar", "role": Role.Dev, "add": true, "edit": true, "view": true, "delete": false, "sidemenu":true},
{"id": 31, "module": "Additional Feature", "role": Role.Dev, "add": true, "edit": false, "view": false, "delete": false, "sidemenu":true},
{"id": 32, "module": "Advanced View", "role": Role.Dev, "add": true, "edit": true, "view": true, "delete": true, "sidemenu":true},
{"id": 33, "module": "JSON Forms", "role": Role.Dev, "add": true, "edit": true, "view": true, "delete": true, "sidemenu":true},
{"id": 34, "module": "File Upload", "role": Role.Dev, "add": true, "edit": true, "view": true, "delete": true, "sidemenu":true},
{"id": 35, "module": "JSON Editor", "role": Role.Dev, "add": true, "edit": true, "view": true, "delete": true, "sidemenu":true},
{"id": 36, "module": "File Upload", "role": Role.Admin, "add": true, "edit": true, "view": true, "delete": true, "sidemenu":true},
{"id": 37, "module": "JSON Editor", "role": Role.Admin, "add": true, "edit": true, "view": true, "delete": true, "sidemenu":true},
{"id": 38, "module": "Advanced View", "role": Role.Admin, "add": true, "edit": true, "view": true, "delete": true, "sidemenu":true},
];


    let realFetch = window.fetch;
    window.fetch = function (url, opts) {
        const authHeader = opts.headers['Authorization'];
        const isLoggedIn = authHeader && authHeader.startsWith('Bearer fake-jwt-token');
        const roleString = isLoggedIn && authHeader.split('.')[1];
        const role = roleString ? Role[roleString] : null;

        return new Promise((resolve, reject) => {
           
            // wrap in timeout to simulate server api call
            setTimeout(() => {
                // authenticate - public
                if (url.endsWith('/users/authenticate') && opts.method === 'POST') {
                    const params = JSON.parse(opts.body);
                    const user = users.find(x => x.username === params.username && x.password === params.password);
                    if (!user) return error('Username or password is incorrect');
                    console.log(userRolePermissions.length)

                    var results = [];
                    
                    for (var i=0 ; i < userRolePermissions.length ; i++)
                    {
                            //console.log('i : '+i+' {'+userRolePermissions[i].role+'  == '+ user.role)
                        if (userRolePermissions[i].role == user.role) {
                            results.push(userRolePermissions[i]);
                        }
                    }

                    const userRolePermis = userRolePermissions.find(x => x.role === user.role);
                    
                    return ok({
                        id: user.id,
                        username: user.username,
                        firstName: user.firstName,
                        lastName: user.lastName,
                        role: user.role,
                        token: `fake-jwt-token.${user.role}`,
                        sidemenu: user.sidemenu,
                        results
                    });
                    
                }


                // get user by id - admin or user (user can only access their own record)
                if (url.match(/\/users\/\d+$/) && opts.method === 'GET') {
                    if (!isLoggedIn) return unauthorised();

                    // get id from request url
                    let urlParts = url.split('/');
                    let id = parseInt(urlParts[urlParts.length - 1]);

                    // only allow normal users access to their own record
                    const currentUser = users.find(x => x.role === role);
                    if (id !== currentUser.id && role !== Role.Admin) return unauthorised();

                    const user = users.find(x => x.id === id);
                    return ok(user);
                    
                }

                // get all users - admin only
                if (url.endsWith('/users') && opts.method === 'GET') {
                    if (role !== Role.Admin) return unauthorised();
                    return ok(users);
                }

                // pass through any requests not handled above
                realFetch(url, opts).then(response => resolve(response));

                // private helper functions

                function ok(body) {
                    resolve({ ok: true, text: () => Promise.resolve(JSON.stringify(body)) })
                }

                function unauthorised() {
                    resolve({ status: 401, text: () => Promise.resolve(JSON.stringify({ message: 'Unauthorised' })) })
                }

                function error(message) {
                    resolve({ status: 400, text: () => Promise.resolve(JSON.stringify({ message })) })
                }
            }, 500);
        });
    }
}