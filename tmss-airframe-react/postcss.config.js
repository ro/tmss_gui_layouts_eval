module.exports = {
    plugins: [
        require('autoprefixer')
    ]
};
module.exports = {
    resolve: {
      extensions: ['.js', '.json', '.ttf'],
    },
    module: {
      rules: [
        {
          test: /\.ttf$/,
          use: ['file-loader'],
        },
      ],
    },
  };