const UIConstants = {
    tooltipOptions: {position: 'left', event: 'hover', className:"p-tooltip-custom"}
}

export default UIConstants;