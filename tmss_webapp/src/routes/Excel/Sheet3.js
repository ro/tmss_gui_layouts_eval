import React from 'react'
import ReactDOM from 'react-dom';
import { HotTable } from '@handsontable/react';
import Handsontable from 'handsontable';

export class Sheet3 extends React.Component {
  constructor(props) {
    super(props);
    this.handsontableData = Handsontable.helper.createSpreadsheetData(6, 10);
  }

  render() {
    return (
      <div>
        <HotTable
          id="hot"
          data={this.handsontableData}
          colHeaders={true}
          rowHeaders= {true}
          colHeaders= {['ID', 'Scheduling Set Name', 'SU ID', 'Column1',]}
          />
      </div>
    );
  }
}