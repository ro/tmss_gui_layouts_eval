import React, { PureComponent } from 'react'
import DataSheet from './../../lib'
import Select from 'react-select'
import './override-everything.css'
import { leftShift } from 'mathjs'

const SheetRenderer = props => {
  const {as: Tag, headerAs: Header, bodyAs: Body, rowAs: Row, cellAs: Cell,
    className, columns, selections, onSelectAllChanged} = props
  return (
    <Tag className={className}>
      <Header className='data-header'>
        <Row>
          <Cell className='action-cell cell'>
            <input
              type='checkbox'
              checked={selections.every(s => s)}
              onChange={e => onSelectAllChanged(e.target.checked)}
            />
          </Cell>
          {columns.map(column => <Cell className='cell' style={{ width: column.width }} key={column.label}>{column.label}</Cell>)}
        </Row>
      </Header>
      <Body className='data-body'>
        {props.children}
      </Body>
    </Tag>
  )
}

const RowRenderer = props => {
  const {as: Tag, cellAs: Cell, className, row, selected, onSelectChanged} = props
  return (
    <Tag className={className}>
      <Cell className='action-cell cell'>
        <input
          type='checkbox'
          checked={selected}
          onChange={e => onSelectChanged(row, e.target.checked)}
        />
      </Cell>
      {props.children}
    </Tag>
  )
}

const CellRenderer = props => {
  const {
    as: Tag, cell, row, col, columns, attributesRenderer,
    selected, editing, updated, style,
    ...rest
  } = props

  // hey, how about some custom attributes on our cell?
  const attributes = cell.attributes || {}
  // ignore default style handed to us by the component and roll our own
  attributes.style = { width: columns[col].width }
  if (col === 0) {
    attributes.title = cell.label
  }

  return (
    <Tag {...rest} {...attributes}>
      {props.children}
    </Tag>
  )
}

export default class OverrideEverythingSheet extends PureComponent {
  constructor (props) {
    super(props)
    this.handleSelect = this.handleSelect.bind(this)
    this.handleSelectAllChanged = this.handleSelectAllChanged.bind(this)
    this.handleSelectChanged = this.handleSelectChanged.bind(this)
    this.handleCellsChanged = this.handleCellsChanged.bind(this)

    this.sheetRenderer = this.sheetRenderer.bind(this)
    this.rowRenderer = this.rowRenderer.bind(this)
    this.cellRenderer = this.cellRenderer.bind(this)

    this.generateitem = this.generateitem.bind(this)
    this.options = [
      { label: 'Scheduling Set 1', value: 'Scheduling Set 1' },
      { label: 'Scheduling Set 2', value: 'Scheduling Set 2' },
      { label: 'Scheduling Set 3', value: 'Scheduling Set 3' },
    ]

    this.state = {
      as: 'table',
      angelvalue1: '',
      columns: [
        { label: 'Project Name'  },
        { label: 'Scheduling Set', width:'15%'  },
        { label: 'Description'  },
        { label: 'Schedule Unit Name' },
        {label: 'Blueprint?'},
        { label: 'Angel 1' },
        { label: 'Time 1' }, 
        { label: 'Angel 2' },
        { label: 'Time 2' },
        { label: 'Angel 3' },
        { label: 'Time 3' },
        {label: 'Action', width:'7%'}
      ],
      grid: [
        [
          { value: 'TMSS UC 1'}, 
          { value: <Select
            autofocus
            openOnFocus
            options={this.options}
          /> }, 
          { value: 'TMSS UC 1' },{ value: '' },{ value: <input
            type='checkbox'
          /> },
          { readOnly: true,value: '1' },
           { value: '9:12:12' },
           { readOnly: true,value: '2' },
           { value: '9:12:12' },
           { readOnly: true,value: '3' },
           { value: '9:12:12' },
           { 
            value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),
            forceComponent: true
            }
        ],
        [
          { value: 'TMSS UC 2'}, 
          { value: <Select
            autofocus
            openOnFocus
            options={this.options}
          /> }, 
          { value: 'TMSS UC 2' },
           { value: '' },
           { value: <input
            type='checkbox'
          /> },
          { value: '1' },
           { value: '9:12:12'
           },
           { value: '2' },
           { value: '9:12:12' },
           { value: '3' },
           { value: '9:12:12' },
           { 
            value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),
            forceComponent: true
            }
        ],
        [
          { value: 'TMSS UC 1'}, 
          { value: <Select
            autofocus
            openOnFocus
            options={this.options}
          /> }, 
          { value: 'TMSS UC 1' },
           { value: '' },
           { value: <input
            type='checkbox'
          /> },
          { value: '' },
           { value: '' },
           { value: '' },
           { value: '' },
           { value: '' },
           { value: '' },
           { 
            value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),
            forceComponent: true
            }
        ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
         [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
         [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        [{ value: 'TMSS UC 1'}, { value: <Select autofocus openOnFocus options={this.options} /> },  { value: 'TMSS UC 1' },{ value: '' },{ value: <input type='checkbox' /> },         
        { value: '' },{ value: '' },{ value: '' },{ value: '' }, { value: '' }, { value: '' },{value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),forceComponent: true} ],
        
        [
          { value: 'TMSS UC 1'}, 
          { value: <Select
            autofocus
            openOnFocus
            options={this.options}
          /> }, 
          { value: 'TMSS UC 1' },
           { value: '' },
           { value: <input
            type='checkbox'
          /> },
          { value: '' },
           { value: '' },
           { value: '' },
           { value: '' },
           { value: '' },
           { value: '' },
           { 
            value: '', component: (<div className={'add-grocery'} >  <div className={'add-button'} onClick={this.generateitem}>Add</div></div> ),
            forceComponent: true
            }
        ],
        
      ],
      selections: [false, false, false, false, false, false, false, false, false, false, false, false, false, false]
    }
  }

   getAngleInput(e) {
    let prpInput = e.target.value;
    console.log('prpInput',prpInput)
    let isDegree = false;
    const degrees = prpInput * 180 / Math.PI;
    if (isDegree) {
        const dd = Math.floor(prpInput * 180 / Math.PI);
        const mm = Math.floor((degrees-dd) * 60);
        const ss = +((degrees-dd-(mm/60)) * 3600).toFixed(0);
        return (dd<10?`0${dd}`:`${dd}`) + ':' + (mm<10?`0${mm}`:`${mm}`) + ':' + (ss<10?`0${ss}`:`${ss}`);
    }   else {
        const hh = Math.floor(degrees/15);
        const mm = Math.floor((degrees - (hh*15))/15 * 60 );
        const ss = +((degrees -(hh*15)-(mm*15/60))/15 * 3600).toFixed(0);
        return (hh<10?`0${hh}`:`${hh}`) + ':' + (mm<10?`0${mm}`:`${mm}`) + ':' + (ss<10?`0${ss}`:`${ss}`);
    }
}

  generateitem(e){
    console.log(e.target.value)
    let defaultrec = [
      { value: 'TMSS UC 1'}, 
      { value: <Select
        autofocus
        openOnFocus
        options={this.options}
      /> }, 
      { value: 'TMSS UC 1' },
       { value: '' },
       { value: <input
        type='checkbox'
      /> },
      { value: '' },
       { value: '' },
       { value: '' },
       { value: '' },
       { value: '' },
       { value: '' },
       { 
        value: '', component: (<div className={'add-grocery'} >  <div className={'add-button'} onClick={this.generateitem}>Add</div></div> ),
        forceComponent: true
        }
    ]
    let  len  = this.state.grid.length;
    let lastrec = this.state.grid[len-1];
    this.state.grid[len-1][11]= { 
      value: '', component: (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Remove</div></div> ),
      forceComponent: true
      }
    console.log(lastrec[11])
   // lastrec[11].component= (<div className={'add-grocery'} >  <div className={'remove-button'} onClick={this.generateitem}>Add</div></div> )
    console.log(lastrec)
    this.state.grid[len-1] = lastrec;
    this.state.grid.push(defaultrec);
    console.log('len',this.state.grid)
  }

  handleSelect (e) {
    this.setState({as: e.target.value})
  }

  handleSelectAllChanged (selected) {
    const selections = this.state.selections.map(s => selected)
    this.setState({selections})
  }

  handleSelectChanged (index, selected) {
    const selections = [...this.state.selections]
    selections[index] = selected
    this.setState({selections})
  }

  handleCellsChanged (changes, additions) {
    const grid = this.state.grid.map(row => [...row])
    changes.forEach(({cell, row, col, value}) => {
      grid[row][col] = {...grid[row][col], value}
    })
    // paste extended beyond end, so add a new row
    additions && additions.forEach(({cell, row, col, value}) => {
      if (!grid[row]) {
        grid[row] = [{value: ''}, {value: ''}, {value: ''}, {value: 0}]
      }
      if (grid[row][col]) {
        grid[row][col] = {...grid[row][col], value}
      }
    })
    this.setState({grid})
  }

  sheetRenderer (props) {
    const {columns, selections} = this.state
    switch (this.state.as) {
      case 'list':
        return <SheetRenderer columns={columns} selections={selections} onSelectAllChanged={this.handleSelectAllChanged} as='segment' headerAs='div' bodyAs='ul' rowAs='div' cellAs='div' {...props} />
      case 'div':
        return <SheetRenderer columns={columns} selections={selections} onSelectAllChanged={this.handleSelectAllChanged} as='div' headerAs='div' bodyAs='div' rowAs='div' cellAs='div' {...props} />
      default:
        return <SheetRenderer columns={columns} selections={selections} onSelectAllChanged={this.handleSelectAllChanged} as='table' headerAs='thead' bodyAs='tbody' rowAs='tr' cellAs='th' {...props} />
    }
  }

  rowRenderer (props) {
    const {selections} = this.state
    switch (this.state.as) {
      case 'list':
        return <RowRenderer as='li' cellAs='div' selected={selections[props.row]} onSelectChanged={this.handleSelectChanged} className='data-row' {...props} />
      case 'div':
        return <RowRenderer as='div' cellAs='div' selected={selections[props.row]} onSelectChanged={this.handleSelectChanged} className='data-row' {...props} />
      default:
        return <RowRenderer as='tr' cellAs='td' selected={selections[props.row]} onSelectChanged={this.handleSelectChanged} className='data-row' {...props} />
    }
  }

  cellRenderer (props) {
    switch (this.state.as) {
      case 'list':
        return <CellRenderer as='div' columns={this.state.columns} {...props} />
      case 'div':
        return <CellRenderer as='div' columns={this.state.columns} {...props} />
      default:
        return <CellRenderer as='td' columns={this.state.columns} {...props} />
    }
  }

  render () {
    return (
      <div>
        <div>
          <label> React - Datasheet  
            
          </label>
        </div>
      <br/>
        <DataSheet
          data={this.state.grid}
          className='custom-sheet'
          sheetRenderer={this.sheetRenderer}
          headerRenderer={this.headerRenderer}
          bodyRenderer={this.bodyRenderer}
          rowRenderer={this.rowRenderer}
          cellRenderer={this.cellRenderer}
          onCellsChanged={this.handleCellsChanged}
          valueRenderer={(cell) => cell.value}
        />
      </div>
    )
  }
}
