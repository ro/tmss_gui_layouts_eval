import React, { useState } from 'react';
import { render } from 'react-dom';
import { AgGridColumn, AgGridReact } from 'ag-grid-react';
import { AllModules } from '@ag-grid-enterprise/all-modules';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import NumericEditor from './numericEditor';

export class AGGrid extends React.Component{
    constructor(props){
        super(props)
        this.gridApi = ''
        this.gridColumnApi = ''
        this.rowData = [{"athlete":"Michael Phelps","age":23,"country":"United States","year":2008,"date":"24/08/2008","sport":"Swimming","gold":8,"silver":0,"bronze":0,"total":8},{"athlete":"Michael Phelps","age":19,"country":"United States","year":2004,"date":"29/08/2004","sport":"Swimming","gold":6,"silver":0,"bronze":2,"total":8},{"athlete":"Michael Phelps","age":27,"country":"United States","year":2012,"date":"12/08/2012","sport":"Swimming","gold":4,"silver":2,"bronze":0,"total":6},{"athlete":"Natalie Coughlin","age":25,"country":"United States","year":2008,"date":"24/08/2008","sport":"Swimming","gold":1,"silver":2,"bronze":3,"total":6},]
        this.onGridReady = this.onGridReady.bind(this);
        
        this.state = {
            modules: AllModules,
            columnDefs: [
                {
                  headerName: 'Scheduling Name',
                  field: 'athlete',
                },
                
                {
                    headerName: 'Description',
                  field: 'country',
                  
                },
                {
                    headerName: 'ID',
                    field: 'id',
                    cellEditor: 'numericCellEditor'
                },
                
                {
                    headerName: 'Target Pointing 0',
                    children: [
                        { headerName: 'Angle 1', field: 'silver' },
                        { headerName: 'Angle 2',field: 'bronze' },
                        { headerName: 'Angle 3',field: 'bronze' },
                        { headerName: 'Direction Type ',field: 'direction_type ' },
                    ]
                },
               {
                   headerName: 'Target Pointing 1',
                    children: [
                    { headerName: 'Angle 1', field: 'silver' },
                    { headerName: 'Angle 2',field: 'bronze' },
                    { headerName: 'Angle 3',field: 'bronze' },
                    { headerName: 'Direction Type ',field: 'direction_type ' },
                    ]
                },
                {
                    headerName: 'Tile beam',
                     children: [
                        { headerName: 'Angle 1', field: 'silver' },
                        { headerName: 'Angle 2',field: 'bronze' },
                        { headerName: 'Angle 3',field: 'bronze' },
                        { headerName: 'Direction Type ',field: 'direction_type ' },
                     ]
                 },
                
              ],
              frameworkComponents: {
                numericEditor: NumericEditor,
              },
              defaultColDef: {
                editable: true,
                flex: 1,
                sortable: true,
                filter: true,
                minWidth: 100,
                resizable: true,
             
              },
              rowSelection: 'multiple',
             // rowData: [{"athlete":"Michael Phelps","age":23,"country":"United States","year":2008,"date":"24/08/2008","sport":"Swimming","gold":8,"silver":0,"bronze":0,"total":8},{"athlete":"Michael Phelps","age":19,"country":"United States","year":2004,"date":"29/08/2004","sport":"Swimming","gold":6,"silver":0,"bronze":2,"total":8},{"athlete":"Michael Phelps","age":27,"country":"United States","year":2012,"date":"12/08/2012","sport":"Swimming","gold":4,"silver":2,"bronze":0,"total":6},{"athlete":"Natalie Coughlin","age":25,"country":"United States","year":2008,"date":"24/08/2008","sport":"Swimming","gold":1,"silver":2,"bronze":3,"total":6},],
             rowData: [
                {"id":0,"athlete":"TMSS UC 1","age":"Scheduling Set 1","country":"TMSS UC ","year":"Scheduling Unit Draft 4","date":"24/08/2008","sport":"Angle 1","gold":"9:54:34","silver":"Angle 2","bronze":0,"total":"Angle 3"},
                {"id":0,"athlete":"TMSS UC 2","age":"Scheduling Set 2","country":"TMSS UC ","year":"Scheduling Unit Draft 2","date":"29/08/2004","sport":"Angle 1","gold":"9:54:34","silver":"Angle 2","bronze":2,"total":"Angle 3"},
                {"id":0,"athlete":"TMSS UC 3","age":"Scheduling Set 3","country":"TMSS UC ","year":"Scheduling Unit Draft 1","date":"12/08/2012","sport":"Angle 1","gold":"9:54:34","silver":"Angle 2","bronze":0,"total":"Angle 3"},
                {"id":0,"athlete":"TMSS UC 3","age":"Scheduling Set 4","country":"TMSS UC ","year":"Scheduling Unit Draft 5" ,"date":"24/08/2008","sport":"Angle 1","gold":"9:54:34","silver":"Angle 2","bronze":3,"total":"Angle 3"},
                { "athlete":"","age":'',"country":"","year":"" ,"date":"","sport":"","gold":"","silver":"","bronze":'',"total":""},
                {"athlete":"","age":'',"country":"","year":"" ,"date":"","sport":"","gold":"","silver":"","bronze":'',"total":""},
                {"athlete":"","age":'',"country":"","year":"" ,"date":"","sport":"","gold":"","silver":"","bronze":'',"total":""},
                {"athlete":"","age":'',"country":"","year":"" ,"date":"","sport":"","gold":"","silver":"","bronze":'',"total":""},
                {"athlete":"","age":'',"country":"","year":"" ,"date":"","sport":"","gold":"","silver":"","bronze":'',"total":""},
                {"athlete":"","age":'',"country":"","year":"" ,"date":"","sport":"","gold":"","silver":"","bronze":'',"total":""},
                {"athlete":"","age":'',"country":"","year":"" ,"date":"","sport":"","gold":"","silver":"","bronze":'',"total":""},
                {"athlete":"","age":'',"country":"","year":"" ,"date":"","sport":"","gold":"","silver":"","bronze":'',"total":""},
                {"athlete":"","age":'',"country":"","year":"" ,"date":"","sport":"","gold":"","silver":"","bronze":'',"total":""},
                {"athlete":"","age":'',"country":"","year":"" ,"date":"","sport":"","gold":"","silver":"","bronze":'',"total":""},
                {"athlete":"","age":'',"country":"","year":"" ,"date":"","sport":"","gold":"","silver":"","bronze":'',"total":""},
                {"athlete":"","age":'',"country":"","year":"" ,"date":"","sport":"","gold":"","silver":"","bronze":'',"total":""},

                ,], 
        }
        this.showcount = this.showcount.bind(this)
    }
    
    onGridReady (params) {
        console.log('params')
        this.setState({
            gridApi:params.api,
            gridColumnApi:params.columnApi
        })
    }

    processCellForClipboard = (params) => {
        console.log('processCellForClipboard')
        return params.value;
      };
    
      processHeaderForClipboard = (params) => {
        console.log('processHeaderForClipboard')
        var colDef = params.column.getColDef();
        var headerName = colDef.headerName || colDef.field;
        if (colDef.headerName == null) {
          headerName = headerName.charAt(0).toUpperCase() + headerName.slice(1);
        }
        return headerName;
      };
    
      processCellFromClipboard = (params) => {
        console.log('processCellFromClipboard')
        return params.value;
      };

      showcount(){
        console.log('row data : ',this.state.rowData)
      }
      numericCellEditor() {
        console.log('mumericCellEditor : ',this.state.rowData)
        }
    render(){
        
        return (
            <div className="ag-theme-alpine" style={ { height: 400,  } }>
                AG Grid 
                <AgGridReact
                    modules={this.state.modules}
                    columnDefs={this.state.columnDefs}
                    defaultColDef={this.state.defaultColDef}
                    enableRangeSelection={true}
                    rowSelection={this.state.rowSelection}
                    processCellForClipboard={this.processCellForClipboard}
                    processHeaderForClipboard={this.processHeaderForClipboard}
                    processCellFromClipboard={this.processCellFromClipboard}
                    onGridReady={this.onGridReady}
                    rowData={this.state.rowData}
                    frameworkComponents={this.state.frameworkComponents}
                 >
                    
                </AgGridReact>
                <div style={{textAlign:'center', marginTop:'10px'}}>
                <button onClick={this.showcount}>Show count</button>
                </div>
            </div>
        );
    }
}
 // pull off row data
export default AGGrid;