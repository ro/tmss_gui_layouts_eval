import React from 'react'
import Spreadsheet from "react-spreadsheet";
import {Dropdown} from 'primereact/dropdown';
import { MultiSelect } from 'primereact/multiselect';
import 'react-datasheet/lib/react-datasheet.css';

export class Sheet1 extends React.Component {

    constructor(props){
        super(props);
        this.options = [
          { label: 'Scheduling Set 1', value: 'Scheduling Set 1' },
          { label: 'Scheduling Set 2', value: 'Scheduling Set 2' },
          { label: 'Scheduling Set 3', value: 'Scheduling Set 3' },
        ]
    }

    render() {
        const RangeView = ({ cell, getValue }) => (
        
            <input
              type="range"
              value={getValue({ data: cell })}
              disabled
              style={{ pointerEvents: "none" }}
            />
          );
           
          const AddButton = () => (
            <input
              type="submit"
              value={'Add Set'}
              onClick={e =>{console.log('testing')}}
            />
          );

          const RemoveButton = () => (
            <input
              type="submit"
              value={'Remove'}
              onClick={e =>{console.log('testing')}}
            />
          );

          const RangeEdit = ({ getValue, cell, onChange }) => (
            <input
              type="range"
              onChange={e => {
                onChange({ ...cell, value: e.target.value });
              }}
              value={getValue({ data: cell }) || 0}
              autoFocus
            />
          );
           
          const DropdownList =  () => (
            <Dropdown inputId="projCat" optionLabel="value" optionValue="url" 
            tooltip="Project Category"  
            options={this.options} 
             />
           );
        
          const data = [
            [{ value: "Project Name" },{ value: "Scheduling Set" },{ value: "Description" },{ value: "Schedule Unit Name Task" }],
            [{ value: "Project-1" }, { value: "Scheduling Set -1" },{ value: 100, DataViewer: DropdownList, DataEditor: DropdownList },{ value: 100, DataViewer: RemoveButton, DataEditor: RemoveButton }],
            [{ value: "Project-2" }, { value: "Scheduling Set -2" },{ value: 100, DataViewer: DropdownList, DataEditor: DropdownList },{ value: 100, DataViewer: RemoveButton, DataEditor: RemoveButton }],
            [{ value: "" }, { value: "" }, { value: "" },{ value: 100, DataViewer: AddButton, DataEditor: AddButton }],
             
          ];
           
          const MyComponent = () => <Spreadsheet data={data} />;
        return (
            <>
               <div>
             <label> React - Spread Sheet  
          </label>
        </div>
                <Spreadsheet data={data}  />
                </>
        );
    }
}