import React, { Component } from 'react';
import { InputMask } from 'primereact/inputmask';

export default class InputMaskRenderer extends Component {
  constructor(props) {
    super(props);

    this.invokeParentMethod = this.invokeParentMethod.bind(this);
  }

  invokeParentMethod() {
      //`Row: ${this.props.node.rowIndex}, Col: ${this.props.colDef.headerName}`
    this.props.context.componentParent.methodFromParent(
      `${this.props}`
    );
  }

  render() {
    return (
      
        <InputMask id="ssn" mask="99:99:99" value={this.props.value} 
        placeholder="HH:mm:ss" style={{height:'35px',width:'80px','text-align': 'center', 'border-top-color': 'transparent',
        'border-bottom-color': 'transparent',
        'border-right-color': 'transparent',
        'border-left-color': 'transparent'}} ></InputMask>
        
      
    );
  }
}