import React, { Component } from 'react';

export default class AddChildMessageRenderer extends Component {
  constructor(props) {
    super(props);

    this.invokeParentMethod = this.invokeParentMethod.bind(this);
  }

  invokeParentMethod() {
      //`Row: ${this.props.node.rowIndex}, Col: ${this.props.colDef.headerName}`
    this.props.context.componentParent.methodFromParent(
      `${this.props}`
    );
  }

  render() {
    return (
      <span>
        <a href="#" style={{ height: 20, lineHeight: 0.5,color: 'green', backgroundColor: 'transparent' }}
        onClick={this.invokeParentMethod} ><i class="fas fa-plus-square"></i></a>
      </span>
    );
  }
}