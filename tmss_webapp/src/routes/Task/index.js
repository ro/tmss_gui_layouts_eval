import {TaskEdit} from './edit';
import {TaskView} from './view';
import {DataProduct} from './dataproduct';

export {TaskEdit, TaskView, DataProduct} ;
